<?php

function ndla_share_shortcuts_block() {
  if(user_is_logged_in()) {
    $content = drupal_get_form('ndla_share_form');
  } 
  else {
    /**
     * The url should be fetched from ndla_auth_login_url()
     * but the function returns a leading slash
     */
    $content = l(t('Login to start sharing'), 'seriaauth/authenticate', array(
      'attributes' => array(
        'class' => 'login-link'
        )
      )
    );
  }
  return array(
    'subject' => '',
    'content' => $content,
  );
}

function ndla_share_secret_link_block() {
  global $base_path, $base_url;
  $path = current_path();
  if(preg_match('/^node\/([\d]+)/', $path, $matches)) {
    $nid = $matches[1];
    $node = node_load($nid);
    if(!$node->status && ndla_share_node_view_access('view', $node)) {
      $salt = variable_get('ndla_share_salt', '');
      $url = $base_url . $base_path . 'node/' . $node->nid;
      $link = l('<i class="fa fa-unlock-alt"></i>', $url, array('html' => true, 'query' => array('key' => md5($nid . $salt))));
      return array(
        'subject' => '',
        'content' => $link,
      );
    }
  }
}

function ndla_share_top_authors_block() {
  global $base_path;
  $path = current_path();
  if(preg_match('/^node\/([\d]+)/', $path, $matches)) {
    $nid = $matches[1];
    $blacklist = variable_get('ndla_share_top_authors_blacklist', array());
    $blacklist[] = 0;
    $query = db_select('field_data_field_common_courses', 'fcc');
    $query->leftjoin('node', 'n', 'fcc.entity_id = n.nid');
    $query->leftjoin('users', 'u', 'n.uid = u.uid');
    $query->leftjoin('field_data_field_display_name', 'd', 'n.uid = d.entity_id');
    $query->addExpression('COUNT(u.uid)', 'count');
    
    $data = $query
      ->fields('u', array('name', 'uid'))
      ->fields('d', array('field_display_name_value'))
      ->condition('fcc.field_common_courses_target_id', $nid)
      ->condition('u.uid', $blacklist, 'NOT IN')
      ->condition('n.status', 1)
      ->groupBy('u.uid')
      ->orderBy('count', 'DESC')
      ->orderBy('u.name', 'ASC')
      ->range(0,10)
      ->execute()
      ->fetchAll();
    $items = array();
    foreach($data as $key => $user) {
      $username = '';
      if(!empty($user->field_display_name_value)) {
        $username = $user->field_display_name_value;
      } else {
        $username = $user->name;
      }
      if($key < 3) {
        $cquery = db_select('field_data_field_courses', 'fc');
        $cquery->leftjoin('node', 'n', 'fc.entity_id = n.nid');
        $cquery->leftjoin('field_data_field_common_courses', 'fcc', 'n.nid = fcc.entity_id');
        $cquery->leftjoin('users', 'u', 'n.uid = u.uid');
        $cquery->leftjoin('field_data_field_display_name', 'd', 'n.uid = d.entity_id');
        $courses = $cquery
          ->fields('u', array('name', 'uid'))
          ->condition('u.uid', $user->uid)
          ->condition('n.status', 1)
          ->condition('fcc.field_common_courses_target_id', $nid)
          ->groupBy('fc.field_courses_target_id')
          ->execute()
          ->rowCount();
        $items[] = array(
          'data' => "<a href='{$base_path}search?f[0]=author:{$user->uid}&f[1]=field_common_courses:{$nid}'>$username</a><br>" . t('!resources shared resources within !areas areas', array('!resources' => $user->count, '!areas' => $courses)),
          'class' => array('position_' . ($key + 1))
        );
      } else {
        $items[] = array(
          'data' => "<a href='{$base_path}search?f[0]=author:{$user->uid}&f[1]=field_common_courses:{$nid}'>$username</a>",
          'class' => array('position_other')
        );
      }
    }
    return array(
      'subject' => t('Top authors'),
      'content' => theme('item_list', array('items' => $items, 'attributes' => array('class' => 'top-authors'))) . l(t('See whole list here') . '<div></div>', 'node/' . $nid . '/top-authors', array('html' => true, 'attributes' => array('class' => array('arrow-button')))),
    );
  }
}

function ndla_share_top_authors_global_block() {
  global $base_path;
  $blacklist = array_filter(variable_get('ndla_share_top_authors_blacklist', array()));
  $blacklist[] = 0;
  $items = array();
  //Too old to use the new DB API.
  $result = db_query("SELECT d.field_display_name_value, u.name, u.uid, count(u.uid) AS num_shares FROM {users} u
            INNER JOIN {node} n ON n.uid = u.uid
            LEFT JOIN {field_data_field_display_name} d ON d.entity_id = n.uid
            WHERE u.name <> '' AND u.uid NOT IN(:blacklisted)
            GROUP BY u.uid ORDER BY num_shares DESC LIMIT 10", array(':blacklisted' => $blacklist));

  $count = 1;
  while($row = $result->fetchObject()) {
    $username = $row->name;
    if(!empty($row->field_display_name_value)) {
      $username = $row->field_display_name_value;
    }
    
    if(strlen($username) > 20) {
      $username = trim(substr($username, 0, 19)) . "...";
    }
    
    if($count < 4) {
      $items[] = array(
        'data' => "<a href='{$base_path}search?f[0]=author:{$row->uid}'>$username</a><br>" . t('!resources shared', array('!resources' => $row->num_shares)),
        'class' => array('position_' . $count)
      );
    }
    else {
      $items[] = array(
        'data' => "<a href='{$base_path}search?f[0]=author:{$row->uid}'>$username</a>",
        'class' => array('position_other')
      );
    }
    
    $count++;
  }
  
  if(!empty($items)) {
    return array(
      'subject' => t('Top authors'),
      'content' => theme('item_list', array('items' => $items, 'attributes' => array('class' => 'top-authors'))) . l(t('See whole list here') . '<div></div>', 'top-authors', array('html' => true, 'attributes' => array('class' => array('arrow-button')))),
    );
  }
}