<?php
/** 
 * Access plugin that provides property based access control. 
 */
class ndla_share_access_plugin extends views_plugin_access {

  function summary_title() {
    return t('Custom acces plugin');
  }

 /** 
  * Determine if the current user has access or not.
  */
  function access($account) {
    return ndla_share_views_access($account);
  }

  function get_access_callback() {
    return array('ndla_share_access', array());
  }
}