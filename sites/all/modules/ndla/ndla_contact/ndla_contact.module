<?php

/**
 * Implements hook_block_info().
 */
function ndla_contact_block_info() {
  $blocks['ndla_contact_jira_collector'] = array(
    'info' => t('Jira Collector'),
    'cache' => DRUPAL_CACHE_PER_PAGE,
  );
  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function ndla_contact_block_view($delta = '') {
  $block = array();
  switch($delta) {
    case 'ndla_contact_jira_collector':
      $block = ndla_contact_jira_collector_block();
      break;
  }
  return $block;
}

function ndla_contact_jira_collector_block() {
  $content = l(t('Report'), 'contact-us', array('attributes' => array('class' => array('ndla-contact-button')), 'query' => array('url' => request_uri())));
  drupal_add_css(drupal_get_path('module', 'ndla_contact') . '/css/ndla_contact.css');
  drupal_add_js(drupal_get_path('module', 'ndla_contact') . '/js/ndla_contact.js');
  return array('subject' => '', 'content' => $content);
}

/**
 * Implements hook_menu().
 */
function ndla_contact_menu() {
  $items = array();
  $items['contact-us'] = array(
    'title' => t('Contact us'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('ndla_contact_form'),
    'access arguments' => array('access content'),
  );
  $items['admin/config/services/ndla_contact'] = array(
    'title' => t('NDLA Jira issue collector'),
    'description' => t('Settings for the Jira issue collector.'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('ndla_contact_settings_form'),
    'access arguments' => array('administer ndla_contact'),
  );
  return $items;
}

function ndla_contact_settings_form() {
  $form = array();
  $form['radio_btn_options'] = array(
    '#title' => t('Radio button options.'),
    '#description' => t('Eachline in the textbox represents an option presented as a radio button. Each option should be piped ie) VALUE|ENGLISH TITLE'), 
    '#type' => 'textarea',
    '#default_value' => variable_get('radio_btn_options',''),
  );
  $form['jirauserpass'] = array(
    '#title' => 'Jira username and password.',
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#description' => 'Type in the username and password that will be used to login to JIRA with.
    this is needed in order to be able post to JIRA.',
   );
  $form['jirauserpass']['jira_user'] = array(
    '#title' => t('User'),
    '#description' => t('The username for the jira account.'),
    '#type' => 'textfield',
    '#default_value' => variable_get('jira_user',''),
  );
  $form['jirauserpass']['jira_password'] = array(
    '#title' => t('Password'),
    '#description' => t('Password for the jira account'),
    '#type' => 'password',
    '#default_value' => variable_get('jira_password',''),
  );
  $form['jirafields'] = array(
    '#title' => 'JIRA fields',
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#description' => 'Please make sure these settings correspond to the correct values in JIRA.',
  );
  $form['jirafields']['jira_project'] = array(
    '#title' => 'Project',
    '#type' => 'textfield',
    '#default_value' => variable_get('jira_project',''),
    '#description' => 'The project key. f.ex. RDM or TGP',
  );
  $form['jirafields']['jira_issuetype'] = array(
    '#title' => 'Issue type',
    '#description' => '',
    '#default_value' => variable_get('jira_issuetype',''),
    '#type' => 'textfield',
  );
  $form['jirafields']['jira_reporter'] = array(
    '#title' => 'Reporter',
    '#description' => 'The user that will be set as reporter',
    '#default_value' => variable_get('reporter',''),
    '#type' => 'textfield',
  );
  $form['google_recaptcha'] = array(
    '#title' => 'Google reCaptcha',
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
  );
  $form['google_recaptcha']['google_recaptcha_enabled'] = array(
    '#title' => 'Enable Google reCaptcha',
    '#type' => 'checkbox',
    '#default_value' => variable_get('google_recaptcha_enabled',''),
  );
  $form['google_recaptcha']['google_pub_key'] = array(
    '#title' => 'Public key',
    '#description' => 'Google reCaptcha public key',
    '#type' => 'textfield',
    '#default_value' => variable_get('google_pub_key',''),
  );
  $form['google_recaptcha']['google_secret_key'] = array(
    '#title' => 'Secret key',
    '#description' => 'Google reCaptcha secret key',
    '#type' => 'textfield',
    '#default_value' => variable_get('google_secret_key',''),
  );
  return system_settings_form($form); 
}

function ndla_contact_form($form_state) {
  $sitekey = variable_get('google_pub_key','');
  $is_mobile = !empty($_REQUEST['mobile']);
  $options = array();
  foreach(explode("\n", str_replace("\r", "", variable_get('radio_btn_options',''))) as $index => $data) {
    $option = array_map('trim', explode("|", $data));
    if(count($option) == 2) {
      $options[$option[0]] = t($option[1]);
    }
    else {
      $options[$option[0]] = $option[0];
    }
  }

  $form['email'] = array(
    '#prefix' => '<script src="https://www.google.com/recaptcha/api.js"></script><div id="ndla_contact_form"><h2 class="block-tab">' . t('Leave us some feedback') . '</h1>',
    '#title' => t('Your e-mail.'),
    '#type' => 'textfield',
    '#size' => '30',
    '#required' => FALSE,
    '#description' => t('If you want us to be able to reply to you, it is important that you enter a valid e-mail address'),
  );
  $form['concern'] = array(
    '#type' => 'radios',
    '#title' => t('Your issue concerns?'),
    '#description' => t('Select the most appropiate option'),
    '#required' => FALSE,
    '#default_value' => "Generelt",
    '#label' => FALSE,
    '#options' => array_filter($options),
  );
  $form['description'] = array(
    '#type' => 'textarea',
    '#resizable' => FALSE,
    '#title' => t('Description'),
    '#description' => t('Type your issue here'),
    '#required' => TRUE,
    '#rows' => 7,
  );
  $form['url'] = array(
    '#default_value' => $_SERVER['HTTP_REFERER'],
    '#type' => 'hidden',
  );
  
  $form['is_mobile'] = array(
    '#type' => 'value',
    '#value' => $is_mobile,
  );

  // Show reCaptcha if enabled
  if(variable_get('google_recaptcha_enabled','')) {
    $form['recaptcha'] = array(
      '#markup' => "<div class='g-recaptcha' data-sitekey=$sitekey></div>",
    );
  }
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Send',
    '#suffix' => '</div>',
  );
  $form['#attributes']['novalidate'] = '1';
  return $form;
}

function ndla_contact_form_validate($form, &$form_state) {
  $google_recaptcha = variable_get('google_recaptcha_enabled','');
  $privkey = variable_get('google_secret_key','');

  if(isset($_POST['g-recaptcha-response'])){
    $captcha=$_POST['g-recaptcha-response'];
  }

  $url="https://www.google.com/recaptcha/api/siteverify?secret=$privkey&response=$captcha";
  $json = file_get_contents($url);
  $res = json_decode($json, true);
  if(!$google_recaptcha || ($google_recaptcha && $res['success'])) {
    $email = $form_state['values']['email'];
    // Only check if e-mail address is valid if something is entered in field.
    if($email && !filter_var($email, FILTER_VALIDATE_EMAIL)) {
      form_set_error('email',t('Please enter a valid e-mail address.'));
    }
  // The reCaptcha test failed. This smells like robot fingers.
  } else {
    form_set_error('', t('Please complete the reCaptcha so we know you are not a robot.'));
  }
}

function ndla_contact_form_submit($form, &$form_state) {
  global $base_url, $base_path;
  $concern = $form_state['values']['concern'];
  $email = $form_state['values']['email'];
  $url = $form_state['values']['url'];
  $description = $form_state['values']['description'];

  $username = variable_get('jira_user','');
  $password = variable_get('jira_password','');
  $projectkey = variable_get('jira_project','');
  $issuetype = variable_get('jira_issuetype','');
  $reporter = variable_get('jira_reporter','');
  if (mb_strlen($description) > 50) {
    $summary = mb_substr($description,0, 49) . "...";
  } else {
    $summary = $description;
  }
  $environment = "URL: " . $base_url . $base_path . $url . " USER AGENT: " . $_SERVER['HTTP_USER_AGENT'];

  $data = array(
    'fields' => array(
      'project' => array(
        'key' => $projectkey
      ),
      'summary' => $summary,
      'description' => $description,
      'issuetype' => array(
        'name' => $issuetype
      ),
      'environment' => $environment,
      'customfield_11491' => array(
       'value' =>  $concern,
      ),
      'customfield_11592' => $email,
    ),
  );
  $response = drupal_http_request('https://' . $username . ':' . $password . '@bug.ndla.no/rest/api/2/issue/', array('headers' => array('Accept' => 'application/json', 'Content-Type' => 'application/json'), 'method' => 'POST', 'data' => json_encode($data)));
  drupal_set_message(t('Thank you for your feedback!'), 'status');
  header('Location: ' . $base_url . $base_path . $url);
}
