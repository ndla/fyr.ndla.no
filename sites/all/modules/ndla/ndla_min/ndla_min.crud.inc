<?php

function ndla_min_set($node, $type) {
  switch($type) {
    case 'favorite':
      $data = array(
        'title' => $node->title,
        'contenttype' => node_type_get_name($node),
        'subject' => '',
      );
      NdlaMinClient::getInstance()->set('scope', $type, $data, $node);
      break;
  }
}

function ndla_min_delete($node, $type) {
  switch($type) {
    case 'favorite':
      NdlaMinClient::getInstance()->delete('page', $type, $node);
      break;
  }
}