<?php

function ndla_min_my_subjects_form() {
  drupal_add_js(drupal_get_path('module', 'ndla_min') .'/js/ndla_min.my_subjects_form.js', 'file');
  $form = array();
  $form['filter'] = array(
    '#type' => 'textfield',
    '#title' => t('Search'),
    '#description' => t('Fill in one subject name at the time') . '<br><br><a href="javascript:void(0);" onClick="ndla_show_all_subjects();">' . t('Show all') . '</a>',
  );
  $form['subjects'] = array(
    '#type' => 'checkboxes',
    '#options' => array(),
    '#default_value' => array(),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  return $form;
}