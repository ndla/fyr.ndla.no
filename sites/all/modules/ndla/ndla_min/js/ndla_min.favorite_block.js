jQuery(document).ready(function($) {

  $('.block-ndla-min-ndla-min-favorite span').click(function() {
    var nid = $(this).attr('class').match(/nid-([\d]*)/)[1];
    var url = Drupal.settings.basePath + Drupal.settings.pathPrefix;
    if($(this).hasClass('marked')) {
      $(this).removeClass('marked');
      url += 'ndla_min/' + nid + '/delete/favorite';
    } else {
      url += 'ndla_min/' + nid + '/set/favorite';
      $(this).addClass('marked');
    }
    $.get(url);
  });

});