jQuery(document).ready(function($) {
  $('#edit-subjects .form-type-checkbox').each(function() {
    if(!$(this).find('input[type=checkbox]:checked').length) {
      $(this).hide();
    }
  });
  $('#edit-filter').keyup(function() {
    var text = $(this).val();
    if(text == '') {
      $('#edit-subjects .form-type-checkbox').hide();
    } else {
      var regexp = new RegExp('(' + text + ')', "i");
      $('#edit-subjects label').each(function() {
        if($(this).html().match(regexp)) {
          $(this).parent().show();
        } else {
          $(this).parent().hide();
        }
      });
    }
  });

  ndla_show_all_subjects = function() {
    $('#edit-filter').val("");
    $('#edit-subjects .form-type-checkbox').show();
  }
});