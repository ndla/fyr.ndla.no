(function ($) {
  Drupal.behaviors.ndla_reports = {
    attach: function (context, settings) {
      $('.result-header span').click(function() {
        $(this).next().toggle();
      });
    }
  };
})(jQuery);