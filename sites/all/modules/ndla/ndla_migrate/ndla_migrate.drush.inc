<?php

function ndla_migrate_drush_command() {
  $items = array();
  $items['ndla-migrate'] = array(
    'description' => 'Migrate all data',
    'callback'    => 'ndla_migrate_migrate'
  );
  /*$items['ndla-migrate-grep'] = array(
    'description' => 'Sets grep data',
    'callback'    => 'ndla_migrate_set_courses'
  );*/

  $items['ndla-migrate-rebuild-taxonomy-tree'] = array(
    'description' => 'Treeify taxonomy vocab occupations',
    'callback'    => 'ndla_migrate_rebuild_taxonomy_tree'
  );
  $items['ndla-migrate-authmap-test'] = array(
    'description' => 'Sets correct authmap hash',
    'callback'    => 'ndla_migrate_authmap_test'
  );
  $items['ndla-migrate-names'] = array(
    'description' => 'Update realnames on users',
    'callback'    => 'ndla_migrate_realnames'
  );
  return $items;
}