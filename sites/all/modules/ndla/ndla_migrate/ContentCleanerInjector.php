<?php

class ContentCleanerInjector extends HTMLPurifier_Injector {
	public $name;
	public function handleElement(&$token) {
		static $prev_token;
		static $checked_urls = array();
		global $check_urls, $base_root, $full_host, $entity_id;
		$dead_urls = array();

		$remove_style_from_tags = array(
			'span', 'p'
		);
		if( in_array($token->name, $remove_style_from_tags) ) {
			$token->attr['style'] = '';
		}


		if( $token->name == 'span' && $prev_token->name == 'p' ) {
			// This whileloop forwards the scope until the end then marks closing
			// tag for removal.
			while($this->forwardUntilEndToken($i, $current, $nesting)) {};
			$current->markForDelete = true;
			$token = false;
		}

		/**********************************
		 * Remove "Updated *******" tags  *
		 **********************************/
		$hit = NULL;
		while($this->forwardUntilEndToken($i, $current, $nesting)) {
			if( stripos($current->data, 'Updated') !== FALSE && strlen($current->data) < 28 ) {
				$current->data = '';
				$hit = true;
			}
		}
		if($hit) {
			$current->markForDelete = true;
			$token = false;
		}
	
		if( $token->name === 'a' ) {
			$replace = array(
				'http://resourcecentre.savethechildren.se' => '',
				'../..' => '',
				'http://193.108.43.49' => '',
			);
			
			$token->attr['href'] = str_replace(array_keys($replace), array_values($replace), $token->attr['href']);

			if($check_urls && !in_array($token->attr['href'], $checked_urls)) {
				$checked_urls[] = $token->attr['href'];
				$url = $token->attr['href'];
				$site = $base_root;

				if($full_host) 
					$site = $full_host;

				$full_url = $site . $url;

				if(preg_match_all('/^http.*/i', $url, $m) !== 0)
					$full_url = $url;

				//*
				$ch = curl_init($full_url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_HEADER, 1);
				curl_setopt($ch, CURLOPT_NOBODY, 1);
				$response = curl_exec ($ch);
				$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
				$header = substr($response, 0, $header_size);
				if($response == FALSE) {
					content_cleaner_dead_link($entity_id, $full_url);
				}
				$headers = explode("\r\n", $header);
				
				if(!empty($headers[0])) {
					if(strpos($headers[0], "40") !== FALSE) {
						content_cleaner_dead_link($entity_id, $full_url);
					}
					if(strpos($headers[0], "50") !== FALSE) {
						content_cleaner_dead_link($entity_id, $full_url);
					}
				}
				//*/
				// content_cleaner_dead_link($entity_id, $full_url);
			}
		}

		if ( $token->name == 'div' ) {
			$token->name = 'p';
			$token->attr['class'] = '';

			while($this->forwardUntilEndToken($i, $current, $nesting)) {};

			$current->markForRename = true;
			$current->newName = 'div';
		}
		$prev_token = $token;
	}

	public function handleEnd(&$token) {
		if($token->markForDelete) {
			$token = false;
		}
		if($token->markForRename) {
			$token->name = $token->newName;
		}
	}
}
