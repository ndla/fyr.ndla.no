<?php

function ndla_communication_room_header_block($gid) {
  $block = array();
  $node = node_load($gid);
  if(!empty($node->field_logo)) {
    $image = file_create_url($node->field_logo['und'][0]['uri']);
  } else {
    $image = base_path() . drupal_get_path('module', 'ndla_communication_room') . "/images/no_logo.jpg";
  }
  $block['subject'] = '';
  $block['content'] = theme('ndla_communication_room_header', array('title' => $node->title, 'nid' => $node->nid, 'image' => $image));
  return $block;
}