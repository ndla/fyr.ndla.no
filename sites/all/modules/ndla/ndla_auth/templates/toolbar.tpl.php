<?php
global $user;
?>
<div id="header">
  <a href="http://ndla.no">
    <img src="/sites/all/modules/ndla/ndla_auth/images/ndla.png" alt="" id="logo" class="logo">
  </a>
	<nav id="navigation">
	  <ul>
	    <li class="submenu">
        <a href="<?php print url('mittarkiv/' . $user->uid) ?>">
          <i class="fa fa-archive"></i><?php print t('My archive') ?>
        </a>
      </li>
      <li class="submenu">        
        <span>
          <div id="user-icon">
            <?php if($image != false): ?>
              <img src="<?php print $image ?>&width=25&height=25&method=scaledowncrop">
            <?php else: ?>
              <i class="fa fa-user"></i>
            <?php endif; ?>
          </div>
          <?php print t('Welcome') ?> <?php print $name ?>
        </span>
		    <ul class="dropdown">
					<li>
            <a href="<?php print $logout_url ?>">
              <i class="fa fa-sign-out"></i><?php print t('Log out') ?>
            </a>
          </li>
				</ul>
			</li>
    </ul>
	</nav>
</div>
