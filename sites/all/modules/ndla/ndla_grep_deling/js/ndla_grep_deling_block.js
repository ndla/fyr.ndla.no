(function($, document) {
  $(function() {
    
    var $block_wrapper = $('.block-ndla-grep-deling-ndla-grep-deling-competence-aims .list_wrapper');
    if($block_wrapper.length == 0) {
      return;
    }
    var $aims = $('.block-ndla-grep-deling-ndla-grep-deling-competence-aims ul .aims');
    var $show_more_button = $('.block-ndla-grep-deling-ndla-grep-deling-competence-aims .show-more');
    
    if($aims.length < 4) {
      $('.bottom_fader').hide();
      return;
    }

    //var element_3 = $aims.eq(2).height();
    //var element_4_top = $aims.eq(3).position().top;
    //var top = $aims.eq(2).position().top + element_3;
    //
    //var height = (top + element_4_top) / 1.9;
    var old_height = $block_wrapper.height();

    var height = 400;
    $block_wrapper.css('overflow', 'hidden');
    $block_wrapper.css('height', height);
    $block_wrapper.addClass('height-transition');

    $show_more_button.click(function(e) {
      e.preventDefault();
      $block_wrapper.css('height', old_height + 15);
      $('.bottom_fader').hide();
    }) 
  })
})(jQuery, document)