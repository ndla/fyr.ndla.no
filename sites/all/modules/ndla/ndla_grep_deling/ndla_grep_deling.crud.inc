<?php

/**
 * Implements hook_node_insert().
 */
function ndla_grep_deling_node_insert($node) {
  $content_types = array_filter(variable_get('ndla_grep_content_types', array()));
  if(in_array($node->type, $content_types)) {
    $result = NdlaGrepDelingClient::save_resource($node);
    if(!$result) {
      drupal_set_message(t('Could not save resource at !host', array('!host' => variable_get('ndla_grep_host', ''))), 'error');
    } else {
      ndla_grep_deling_node_update_aims($node);
    }
  }
}

/**
 * Implements hook_node_update().
 */
function ndla_grep_deling_node_update($node) {
  $content_types = array_filter(variable_get('ndla_grep_content_types', array()));
  if(in_array($node->type, $content_types) && isset($node->ndla_grep_data)) {
    ndla_grep_deling_node_update_aims($node);
  }
}

function ndla_grep_deling_node_update_aims($node) {
  NdlaGrepDelingClient::delete_aims($node);
  $new_aims = array();
  if(!empty($node->ndla_grep_data)) {
    $data = json_decode($node->ndla_grep_data, TRUE);
    foreach($data as $curriculum) {
      foreach(array_intersect_key($curriculum, array_flip(element_children($curriculum))) as $competence_aim_set) {
        foreach(array_intersect_key($competence_aim_set, array_flip(element_children($competence_aim_set))) as $main_group) {
          foreach(array_intersect_key($main_group, array_flip(element_children($main_group))) as $competence_aim) {
            $new_aims[] = array(
              'id' => $competence_aim['#id'],
              'curriculum_id' => $curriculum['#id'],
              'competence_aim_set_id' => $main_group['#id'],
            );
          }
        }
      }
    }
  }
  NdlaGrepDelingClient::save_aims($node, $new_aims);
}

/**
 * Implements hook_node_delete().
 */
function ndla_grep_deling_node_delete($node) {
  NdlaGrepDelingClient::delete_aims($node);
}