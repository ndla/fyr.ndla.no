<?php

class NdlaGrepSolrClient {

  public static function search($string) {
    global $language;
    $lang = NdlaGrepLanguages::iso639_1_iso639_2($language->language);

    $url = variable_get('ndla_grep_solr', '');
    $url .= '/select/?fq=type:curriculum&wt=json&rows=1000&q=' . $string;
    $url .= '&qf=title&qf=title_' . $lang;

    $data = cache_get($url, 'cache_ndla_grep');
    if(!empty($data->data)) {
      return $data->data;
    }
    $response = drupal_http_request($url, array('headers' => array('Accept' => 'application/json')));
    $data = json_decode($response->data);
    $out = array();
    foreach($data->response->docs as $curricula) {
      $name = self::_extract_name($curricula);
      $out[$name . uniqid()] = array(
        'name' => $name . ' (' . $curricula->id[0] . ')',
        'id' => $curricula->id[0],
        'endpoint' => 'curriculums/' . $curricula->id[0],
      );
    }
    ksort($out);
    cache_set($url, $out, 'cache_ndla_grep');
    return $out;
  }

  public static function get_curricula() {
    global $language;
    $url = variable_get('ndla_grep_solr', '');
    $url .= '/select/?fq=type:curriculum&wt=json&rows=1000&q.alt=*:*';
    $data = cache_get($url . ':' . $language->language, 'cache_ndla_grep');
    if(!empty($data->data)) {
      return $data->data;
    }
    $response = drupal_http_request($url, array('headers' => array('Accept' => 'application/json')));
    $data = json_decode($response->data);
    $out = array();
    foreach($data->response->docs as $curricula) {
      $name = self::_extract_name($curricula);
      $out[$curricula->id['0']] = $name;
    }
    asort($out);
    cache_set($url . ':' . $language->language, $out, 'cache_ndla_grep');
    return $out;
  }

  private static function _extract_name($names) {
    global $language;
    $lang = NdlaGrepLanguages::iso639_1_iso639_2($language->language);
    if(!empty($names->{'title_' . $lang})) {
      return $names->{'title_' . $lang};
    }
    if(!empty($names->title)) {
      return $names->title;
    }
    if(!empty($names->title_nob)) {
      return $names->title_nob;
    }
    if(!empty($names->title_nno)) {
      return $names->title_nno;
    }
    if(!empty($names->title_eng)) {
      return $names->title_eng;
    }
    return "N/A";
  }

}