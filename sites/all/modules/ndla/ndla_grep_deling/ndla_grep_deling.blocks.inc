<?php

/**
 * Implements hook_block_info().
 */
function ndla_grep_deling_block_info() {
  $blocks['ndla_grep_deling_competence_aims'] = array(
    'info' => t('NDLA Grep: Competence aims'),
    'cache' => DRUPAL_CACHE_PER_PAGE,
  );
  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function ndla_grep_deling_block_view($delta = '') {
  $block = array();
  switch($delta) {
    case 'ndla_grep_deling_competence_aims':
      $block = ndla_grep_deling_competence_aims_block();
      break;
  }
  return $block;
}

function ndla_grep_deling_competence_aims_block() {  
  global $base_path;
  $path = current_path();
  if(!preg_match('/^node\/([\d]+)$/', $path, $matches)) {
    return;
  }
  $nid = $matches[1];
  $node = node_load($nid);

  /* Return if content type is not enabled */
  $content_types = array_filter(variable_get('ndla_grep_content_types', array()));
  if(!in_array($node->type, $content_types)) {
    return;
  }

  global $user;
  $account = user_load($user->uid);
  if(!node_access('view', $node, $account)) {
    return;
  }

  if($node) {
    $aims = NdlaGrepDelingClient::load_aims($node);
  }

  if(empty($aims)) {
    return;
  }

  $list = array();
  $curriculum_list = array();
  foreach($aims as $curriculum) {
    $main_group_list = array();
    foreach(array_intersect_key($curriculum, array_flip(element_children($curriculum))) as $competence_aim_set) {
      foreach(array_intersect_key($competence_aim_set, array_flip(element_children($competence_aim_set))) as $main_group) {
        $aim_list = array();
        foreach(array_intersect_key($main_group, array_flip(element_children($main_group))) as $aim) {
          $aim_list[] = array('class'=> array('aims'),'data' => '<a href="' . $base_path . 'search?f[0]=ndla_grep_competence_aims_id:' . urlencode($aim['#id']) . '" class="aim">' . $aim['#name'] . '</a>');
        }
        $main_group_list[] = array(
          'data' => theme('html_tag', array('element' => array('#tag' => 'span', '#attributes' => array('class' => 'subtitle'), '#value' => $main_group['#name']))),
          'children' => $aim_list,
        );
      }
    }
    $curriculum_list[] = array(
      'data' => theme('html_tag', array('element' => array('#tag' => 'span', '#attributes' => array('class' => 'title'), '#value' => $curriculum['#name']))),
      'children' => $main_group_list,
    );
  }

  /* Render output */
  drupal_add_js(drupal_get_path('module', 'ndla_grep_deling') . '/js/ndla_grep_deling_block.js', 'file');
  $list_content = theme('item_list', array('attributes' => array('class' => 'course_list'),'items' => $curriculum_list));

  $wrapper_html = "<div class='list_wrapper'>" . $list_content  ."</div>";
  $bottom_html = "
    <div class='bottom_fader'>
      <div class='transparent'></div>
      <a class='show-more' href='#'>" . t('Show more') . "</a>
    </div>";
  return array('subject' => '', 'content' => $wrapper_html . $bottom_html);
}