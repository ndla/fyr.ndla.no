(function ($) {
  
  Drupal.behaviors.ndla_communication_room = {
    attach: function (context, settings) {  
      
      var canvas = {
        height: $('#canvas').height(),
        width: $('#canvas').width(),
      };
      if(canvas.height == 0) {
        canvas.height = Math.round(canvas.width*0.5);
      }
      
      var item = {
        height: $('#canvas #hub').height(),
        width: $('#canvas #hub').width(),
      };
  
      /* Set position of hub */
      var x = Math.round((canvas.width/2) - (item.width/2));
      $('#canvas #hub').css('left', x + 'px');
      var y = Math.round((canvas.height/2) - (item.height/2));
      $('#canvas #hub').css('top', y + 'px');
      
      /* Get degree per item */
      var degrees = (Math.PI * 2 / $('#canvas .item:not(#hub)').length);
      //Invert to make the circle go clockwise
      degrees *= -1;
      
      var svg = Raphael('canvas', canvas.width, canvas.height);
      
      /* Position item and draw line */
      $('#canvas .item:not(#hub)').each(function(key) {
        var modifier = 0.8;
        var w = (canvas.width/2);
        var h = (canvas.height/2);
        var d = degrees * (key);
        
        var position = {
          x: Math.cos(d) * w * modifier,
          y: Math.sin(d) * h * modifier,
          key : key,
        }
        $(this).css('left', w - ($(this).width()/2) + position.x + 'px');
        $(this).css('top', h - ($(this).height()/2) - position.y + 'px');
        line = svg.path('M' + w + ',' + h + ' L' + (w + position.x) + ',' + (h - position.y)).show();
        line.attr({stroke: 'black', "stroke-width": 0.5})
      });
      
    },
  };
  
})(jQuery);