<?php

class SeriaAuthModule
{
	protected static $automaticDiscoveryDisabled = false;

	protected static function getDrupalMajorVersion()
	{
		static $major = null;

		if ($major !== null)
			return $major;

		if (defined('VERSION'))
			$version = VERSION;
		else
			return ($major = 0);

		list($major) = explode('.', $version);
		$major = trim($major);
		$major = intval($major);
		return $major;
	}

	/**
	 * parse_str that is not affectd by magic quotes.
	 *
	 * @param unknown_type $str
	 * @param unknown_type $query
	 */
	public static function parse_str($str, &$query)
	{
		parse_str($str, $query);

		if (get_magic_quotes_gpc()) {
			/*
			 * Need to stripslashes if magic quotes are enabled. (parse_str) 
			 */
			$process = array(&$query);
			while (list($key, $val) = each($process)) {
				foreach ($val as $k => $v) {
					unset($process[$key][$k]);
					if (is_array($v)) {
						$process[$key][stripslashes($k)] = $v;
						$process[] = &$process[$key][stripslashes($k)];
					} else {
						$process[$key][stripslashes($k)] = stripslashes($v);
					}
				}
			}
			unset($process);
		}
	}

	/**
	 * Backwards compatible drupal_goto with d6.
	 *
	 * @param unknown_type $path
	 * @param unknown_type $query
	 * @param unknown_type $fragment
	 * @param unknown_type $http_response_code
	 */
	public static function drupal_goto($path = '', $query = NULL, $fragment = NULL, $http_response_code = 302)
	{
		$major = self::getDrupalMajorVersion();
		if ($major >= 7) {
			$options = array();
			if ($query !== NULL) {
				if (is_string($query)) {
					self::parse_str($query, $q);
					$query = $q;
				}
				$options['query'] = $query;
			}
			if ($fragment !== NULL)
				$options['fragment'] = $fragment;
			drupal_goto($path, $options, $http_response_code);
		} else
			drupal_goto($path, $query, $fragment, $http_response_code);
	}

	protected static function getCustomizationObject()
	{
		static $customCodeObject = null;

		if ($customCodeObject === null) {
			$filename = dirname(__FILE__).'/custom.php';
			$customCodeObject = false;
			if (file_exists($filename)) {
				require($filename);
				if (class_exists('SeriaAuthCustomCode'))
					$customCodeObject = new SeriaAuthCustomCode();
			}
		}
		return $customCodeObject;
	}
	protected static function callCustomCode($callbackName, $args=array())
	{
		$customCodeObject = self::getCustomizationObject();
		if ($customCodeObject === false)
			return null;
		if (is_callable(array($customCodeObject, $callbackName)))
			return call_user_func_array(array($customCodeObject, $callbackName), $args);
		else
			return null;
	}
	protected static function getCustomConfig($name, $default=null)
	{
		$customCodeObject = self::getCustomizationObject();
		if ($customCodeObject === false)
			return $default;
		if (isset($customCodeObject->$name))
			return $customCodeObject->$name;
		else
			return $default;
	}
	

	protected static function getStoredSites()
	{
		return variable_get('seriaauth_sites', array());
	}
	protected static function getEnabledSites()
	{
		$sites = self::getStoredSites();
		foreach ($sites as $key => $params) {
			if (!$params['enabled'])
				unset($sites[$key]);
		}
		return $sites;
	}
	public static function addSite(array $site)
	{
		$sites = self::getStoredSites();
		foreach ($sites as $wb) {
			if ($wb['host'] == $site['host'])
				throw new SERIA_Exception('The host does already exist.');
		}
		$sites[] = $site;
		variable_set('seriaauth_sites', $sites);
	}
	public static function removeSite($host)
	{
		$sites = self::getStoredSites();
		foreach ($sites as $key => $site) {
			if ($site['host'] == $host) {
				unset($sites[$key]);
				variable_set('seriaauth_sites', $sites);
				return;
			}
		}
		throw new Exception('The host does not exist.');
	}
	public static function updateSite(array $site)
	{
		$sites = self::getStoredSites();
		foreach ($sites as &$upd) {
			if ($site['host'] == $upd['host']) {
				unset($site['host']);
				foreach ($site as $key => $val)
					$upd[$key] = $val;
				variable_set('seriaauth_sites', $sites);
				return;
			}
		}
		throw new Exception('The host does not exist.');
	}
	public static function getSites()
	{
		$sites = array();
		$stored = self::getStoredSites();
		foreach ($stored as $id => $params) {
			if ($params['enabled'])
				$sites[$id] = $params['name'];
		}
		return $sites;
	}

	public static function redirectTo($url)
	{
		header('Location: '.$url);
		die();
	}

	public static function getSsoExternalScripts()
	{
		$scripts = array();
		$sites = self::getSites();
		foreach ($sites as $id => $siteName) {
			$scripts[] = self::getAuthBaseUrl($id).'/?route=components/authproviders/ssobyjs&siteType=drupal';
		}
		return $scripts;
	}

	public static function menuAuthenticateTitle()
	{
		return 'Seria Platform external authentication';
	}
	public static function loginValidate($form, &$form_state)
	{
		print_r($form_state);
		die("\n\nloginValidate\n");
	}
	public static function formAlter(&$form, $form_state, $form_id)
	{
		if ($form_id == 'user_login_block' || $form_id == 'user_login') {
			if (!self::getSites())
				return;
			if (!empty($form_state['post']['identity'])) {
				$form['name']['#required'] = FALSE;
				$form['pass']['#required'] = FALSE;
				unset($form['#submit']);
				$form['#validate'] = array('seriaauth_login_validate');
			}
			$items = array();
    		$items[] = array(
				'data' => l(t('Seria login'), 'seriaauth/authenticate', array(
					'attributes' => array(
						'title' => t('Log in using a Seria Platform site')
					)
				)),
				'class' => 'seriaauth-link',
			);
    		$form['seriaauth_links'] = array(
				'#value' => theme('item_list', $items),
				'#weight' => 1,
			);
			$form['seriaauth.return_to'] = array('#type' => 'hidden', '#value' => url('seriaauth/authenticate', array('absolute' => TRUE, 'query' => drupal_get_destination())));
		} elseif ($form_id == 'user_register' && isset($_SESSION['seriaauth']['values'])) {
			$form['name']['#default_value'] = $_SESSION['seriaauth']['values']['name'];
			$form['mail']['#default_value'] = $_SESSION['seriaauth']['values']['mail'];
			/*
			 * Hide the password field if we do email verification.
			 */
			if (!variable_get('user_email_verification', TRUE)) {
				$form['pass']['#type'] = 'hidden';
				$form['pass']['#value'] = user_password();
			}
			$form['identity'] = array('#type' => 'hidden', '#value' => $_SESSION['seriaauth']['values']['identity']);
		}
		return $form;
	}
	public static function setUserAuthValues($uid, $values)
	{
		if (self::getDrupalMajorVersion() >= 7) {
			db_merge('seriaauth_userdata')
				->key(array('uid' => $uid))
				->fields(array(
					'data' => serialize($values),
				))
	  			->execute();
		} else {
			$data = serialize($values);
			$result = db_query("INSERT INTO {seriaauth_userdata}
				(uid, data)
				VALUES ('%s', '%s')
				ON DUPLICATE KEY UPDATE data = '%s'",
				$uid, $data, $data
			);
		}
	}
	public static function getUserAuthValue($uid, $name=NULL)
	{
		$res = db_select('seriaauth_userdata', 'd')
			->fields('d')
			->condition('uid', $uid, '=')
			->execute();
		$res = $res->fetchAssoc();
		if ($res) {
			$res = unserialize($res['data']);
			if ($name !== NULL) {
				if (isset($res[$name]))
					return $res[$name];
				else
					return NULL;
			} else
				return $res;
		}
		return NULL;
	}
	public static function getExistingUser($id, $params, $data)
	{
		$major = self::getDrupalMajorVersion();
		self::$automaticDiscoveryDisabled = true;
		$identity = sha1($data['data']['username'].'@'.$params['host']);
		$account = user_external_load($identity);
		if ($account && isset($account->uid) && $account->uid) {
			/*
			 * Found drupal user.
			 */
			if (!$account->login) {
				if (isset($data['verifiedEmails']) && $data['verifiedEmails'] && $account->mail) {
					if (in_array($account->mail, $data['verifiedEmails'])) {
						/*
						 * User has a trusted email account. No need to verify it again.
						 */
						return array('user' => $account, 'login' => true);
					}
				}
				return array('user' => $account, 'login' => false);
			}
			return array('user' => $account, 'login' => true);
		}
		/*
		 * We will not search for matching accounts by email if email-verification is off.
		 * Searching by untrusted email addresses is unsafe in the way that users registering
		 * accounts with incorrect email-addresses are vulnerable to account takeovers.
		 *
		 * It is still not possible to take over accounts with a correct email address.
		 * (Because we are only using verified email addresses gathered from the
		 *  authentication server to search for existing accounts.)
		 *
		 * Attack:
		 * 1. User Bob registered with email adress wohoo_typo@hotmail.com
		 * 2. Alice does register the adress wohoo_typo@hotmail.com and gains control over
		 *    this address.
		 * 3. Alice registers this address as verified with the authentication server and
		 *    tries to log herself in to drupal.
		 * 4. This Drupal-module notices the account that is already registered with this email address
		 *    and and gives Alice access to the account.
		 * 5. Alice has gained access to Bob's account.
		 *
		 * This attack is prevented by the user_email_verification check here.
		 *
		 * Still you can disable this check (and therefore enable the attack) by setting the
		 * allow_takeover_by_verified_email.
		 */
		if (isset($data['verifiedEmails']) && $data['verifiedEmails'] && (variable_get('allow_takeover_by_verified_email', FALSE) || (variable_get('user_email_verification', TRUE) && !variable_get('autoregister_external_user_without_email_verify', FALSE)))) {
			foreach ($data['verifiedEmails'] as $email) {
				if ($major >= 7)
					$account = user_load_by_mail($email);
				else
					$account = user_load(array(
						'mail' => $email
					));
				if ($account && isset($account->uid)) {
					/*
					 * Found drupal user.
					 */
					user_set_authmaps($account, array(
						$identity.'_seriaauth' => $identity
					));
					return array('user' => $account, 'login' => true);
				}
			}
		}
		/*
		 * Extremely naive mode: Allow takeover without verified email by
		 * enabling dangerous_naivemode_account_takeover_by_fake_email
		 * ADVICE: DO NOT ENABLE!
		 */
		if (variable_get('dangerous_naivemode_account_takeover_by_fake_email', FALSE) && variable_get('accept_ignore_security_warning', FALSE)) {
			if ($major >= 7)
				$account = user_load_by_mail($email);
			else
				$account = user_load(array(
					'mail' => $email
				));
			if ($account && isset($account->uid)) {
				/*
				 * Found drupal user.
				 */
				// Sorry I don't want to make this permanent!:
				//user_set_authmaps($account, array(
				//	$identity.'_seriaauth' => $identity
				//));
				return array('user' => $account, 'login' => true);
			}
		}
		return false;
	}
	public static function rpcAuthenticated($id, $params, $data, $roamauth=null)
	{
		global $user;
		$major = self::getDrupalMajorVersion();

		self::$automaticDiscoveryDisabled = true;
		$seriaauthSessionData = $data['data'];
		$seriaauthSessionData['hostname'] = $params['host'];
		$seriaauthSessionData['authBaseUri'] = self::getAuthBaseUrl($id);
		if ($roamauth !== null)
			$seriaauthSessionData['roamauth'] = $roamauth;
		$identity = sha1($data['data']['username'].'@'.$params['host']);
		$account = user_external_load($identity);
		if (user_is_logged_in()) {
			if ($user->uid === $account->uid)
				return true;
			else
				user_logout();
		}

		$existing = self::getExistingUser($id, $params, $data);
		if ($existing) {
			if ($existing['login']) {
				$account = $existing['user'];
				if ($major >= 7) {
					$form_state['uid'] = $account->uid;
					user_login_submit(array(), $form_state);
				} else
					user_external_login($account);
				$_SESSION['authenticatedSiteId'] = $id;
				$_SESSION['seriaauth']['sessiondata'] = $seriaauthSessionData;
				self::setUserAuthValues($account->uid, $seriaauthSessionData);
				self::callCustomCode('loggedIn', array($account, true /* first login */));
				return true;
			} else {
				drupal_set_message(t('Please verify your email address by following the instuctions in the email you received.'), 'warning');
				return false;
			}
		}

		/* prepare data */
		$userdata = array(
			'firstName' => $data['data']['firstName'],
			'lastName' => $data['data']['lastName'],
			'displayName' => $data['data']['displayName'],
			'email' => $data['data']['email'],
			'verifiedEmail' => false,
			'name' => $data['data']['email'],
			'autogen' => false
		);
		/*
		 * Semantics:
		 *  - Required fields to continue: email, displayName
		 *
		 * Just try to get that sorted out....
		 */
		$saveok = true;
		if (!$userdata['firstName'] || !$userdata['lastName'])
			$saveok = false;
		if (!$userdata['displayName']) {
			if ($userdata['firstName'] && $userdata['lastName'])
				$userdata['displayName'] = $userdata['firstName'].' '.$userdata['lastName'];
			else if ($userdata['firstName'])
				$userdata['displayName'] = $userdata['firstName'];
			else if ($userdata['lastName'])
				$userdata['displayName'] = $userdata['lastName'];
		}
		if (!$userdata['email'] && variable_get('avoid_user_register_form_by_reg_without_valid_email', FALSE)) {
			$userdata['autogen'] = array('name' => $userdata['name'], 'email' => $userdata['email']);
			$randstr = substr(sha1(mt_rand().mt_rand()), 0, 4);
			if ($userdata['displayName'])
				$userdata['name'] = $userdata['displayName'];
			else
				$userdata['name'] = 'inval'.$randstr;
			$userdata['email'] = 'inval'.$randstr.'@autoreg.invalid.ndla.no';
		}
		if (isset($data['verifiedEmails']) && $data['verifiedEmails']) {
			if (!in_array($userdata['email'], $data['verifiedEmails']))
				list($userdata['email']) = array_values($data['verifiedEmails']);
			$userdata['verifiedEmail'] = true;
		} else {
			/*
			 * Check for conflict:
			 */
			if ($major >= 7)
				$account = user_load_by_mail($userdata['email']);
			else
				$account = user_load(array(
					'mail' => $userdata['email']
				));
			if ($account && isset($account->uid)) {
				if (variable_get('avoid_user_register_form_by_reg_without_valid_email', FALSE)) {
					/*
					 * Email conflict... trying to hack around it.
					 */
					$userdata['autogen'] = array('name' => $userdata['name'], 'email' => $userdata['email']);
					$randstr = substr(sha1(mt_rand().mt_rand()), 0, 4);
					if ($userdata['displayName'])
						$userdata['name'] = $userdata['displayName'];
					else
						$userdata['name'] = 'inval'.$randstr;
					$userdata['email'] = 'inval'.$randstr.'@autoreg.invalid.ndla.no';
				} else
					$saveok = false;
			}
			if ($major >= 7)
				$account = user_load_by_name($userdata['name']);
			else
				$account = user_load(array(
					'name' => $userdata['name']
				));
			if ($account && isset($account->uid)) {
				if (variable_get('avoid_user_register_form_by_reg_without_valid_email', FALSE)) {
					/*
					 * Name conflict... trying to hack around it.
					 */
					if (!$userdata['autogen'])
						$userdata['autogen'] = array('name' => $userdata['name'], 'email' => $userdata['email']);
					$randstr = substr(sha1(mt_rand().mt_rand()), 0, 4);
					if ($userdata['firstName'])
						$userdata['name'] = $userdata['firstName'].' '.$randstr;
					else if ($userdata['displayName'])
						$userdata['name'] = $userdata['displayName'].' '.$randstr;
					else
						$userdata['name'] = 'inval'.$randstr;
				} else
					$saveok = false;
			}
		}
		/* Register new user */
		if ($saveok) {
			$save = array(
				'name' => $userdata['name'],
				'mail' => $userdata['email'],
				'pass' => user_password(),
				'status' => 1
			);
			$account = new stdClass();
			$account->uid = '';
			$account = user_save($account, $save);
			if ($account && $account->uid) {
				user_set_authmaps($account, array(
					$identity.'_seriaauth' => $identity
				));
				if ($major >= 7) {
					$form_state['uid'] = $account->uid;
					user_login_submit(array(), $form_state);
				} else
					user_external_login($account);
				$_SESSION['authenticatedSiteId'] = $id;
				$_SESSION['seriaauth']['sessiondata'] = $seriaauthSessionData;
				self::setUserAuthValues($account->uid, $seriaauthSessionData);
				self::callCustomCode('loggedIn', array($account, true /* first login */));
				self::redirectTo($continue);
			}
		}
		if (variable_get('user_register', 1)) {
			if ($major >= 7)
				$form_state = form_state_defaults();
			$form_state['redirect'] = null;
			$form_state['values']['name'] = $userdata['email'];
			$form_state['values']['mail'] = $userdata['email'];
			$form_state['values']['pass']  = user_password();
			$form_state['values']['status'] = variable_get('user_register', 1) ? 1 : 0;
			$form_state['values']['response'] = $data;
			$form_state['values']['identity'] = $identity;
			$form_state['values']['provider'] = $id;
			if (!$userdata['email'] || !$userdata['displayName']) {
				$success = false;
			} else if (!$userdata['verifiedEmail'] && !variable_get('autoregister_external_user_without_email_verify', FALSE) && variable_get('user_email_verification', TRUE)) {
				$success = false;
			} else {
				if ($major >= 7)
					$formGenerator = 'seriaauth_drupal7_user_register_form_without_admin_access';
				else
					$formGenerator = 'user_register';
				$form = drupal_retrieve_form($formGenerator, $form_state);
				if ($major >= 7) {
					foreach ($form['account'] as $name => &$element) {
						if (is_array($element))
							$element['#parents'] = array($name);
					}
					unset($element);
				}
				drupal_prepare_form($formGenerator, $form, $form_state);
				drupal_validate_form($formGenerator, $form, $form_state);
				$success = !form_get_errors();
				if (!$success) {
					drupal_set_message(t('Account registration using the information provided by your OpenID provider failed due to the reasons listed below. Please complete the registration by filling out the form below. If you already have an account, you can <a href="@login">log in</a> now and add your OpenID under "My account".', array('@login' => url('user/login'))), 'warning');
					// Append form validation errors below the above warning.
					$messages = drupal_get_messages('error');
					foreach ($messages['error'] as $message) {
						drupal_set_message( $message, 'error');
					}
				}
			}
			if ($success) {
				unset($form_state['values']['response']);
				$account = user_save('', $form_state['values']);
				if (!$account) {
					drupal_set_message(t("Error saving user account."), 'error');
					return false;
				}
				user_set_authmaps($account, array(
					$identity.'_seriaauth' => $identity
				));
				if ($major >= 7) {
					$form_state['uid'] = $account->uid;
					user_login_submit(array(), $form_state);
				} else
					user_external_login($account);
				$_SESSION['authenticatedSiteId'] = $id;
				$_SESSION['seriaauth']['sessiondata'] = $seriaauthSessionData;
				self::setUserAuthValues($account->uid, $seriaauthSessionData);
				self::callCustomCode('loggedIn', array($account, true /* first login */));
				return true;
			}
		} else {
			drupal_set_message(t('Only administrators can create new user accounts.'), 'error');
		}
		return false;
	}
	public static function authenticated($id, $params, $data, $continue, $roamauth=null)
	{
		$major = self::getDrupalMajorVersion();

		self::$automaticDiscoveryDisabled = true;
		$seriaauthSessionData = $data['data'];
		$seriaauthSessionData['hostname'] = $params['host'];
		$seriaauthSessionData['authBaseUri'] = self::getAuthBaseUrl($id);
		$seriaauthSessionData['validatedAt'] = time();
		if ($roamauth !== null)
			$seriaauthSessionData['roamauth'] = $roamauth;
		$identity = sha1($data['data']['username'].'@'.$params['host']);

		$existing = self::getExistingUser($id, $params, $data);
		if ($existing) {
			if ($existing['login']) {
				$account = $existing['user'];
				if ($major >= 7) {
					$form_state['uid'] = $account->uid;
					user_login_submit(array(), $form_state);
				} else
					user_external_login($account);
				$_SESSION['authenticatedSiteId'] = $id;
				$_SESSION['seriaauth']['sessiondata'] = $seriaauthSessionData;
				self::setUserAuthValues($account->uid, $seriaauthSessionData);
				self::callCustomCode('loggedIn', array($account, true /* first login */));
				self::redirectTo($continue);
				return;
			} else {
				drupal_set_message(t('Please verify your email address by following the instuctions in the email you received.'), 'warning');
				self::drupal_goto();
			}
		}

		/* prepare data */
		$userdata = array(
			'firstName' => $data['data']['firstName'],
			'lastName' => $data['data']['lastName'],
			'displayName' => $data['data']['displayName'],
			'email' => $data['data']['email'],
			'verifiedEmail' => false,
			'name' => $data['data']['email'],
			'autogen' => false
		);
		/*
		 * Semantics:
		 *  - Required fields to continue: email, displayName
		 *
		 * Just try to get that sorted out....
		 */
		$saveok = true;
		if (!$userdata['firstName'] || !$userdata['lastName'])
			$saveok = false;
		if (!$userdata['displayName']) {
			if ($userdata['firstName'] && $userdata['lastName'])
				$userdata['displayName'] = $userdata['firstName'].' '.$userdata['lastName'];
			else if ($userdata['firstName'])
				$userdata['displayName'] = $userdata['firstName'];
			else if ($userdata['lastName'])
				$userdata['displayName'] = $userdata['lastName'];
		}
		if (!$userdata['email'] && variable_get('avoid_user_register_form_by_reg_without_valid_email', FALSE)) {
			$userdata['autogen'] = array('name' => $userdata['name'], 'email' => $userdata['email']);
			$randstr = substr(sha1(mt_rand().mt_rand()), 0, 4);
			if ($userdata['displayName'])
				$userdata['name'] = $userdata['displayName'];
			else
				$userdata['name'] = 'inval'.$randstr;
			$userdata['email'] = 'inval'.$randstr.'@autoreg.invalid.ndla.no';
		}
		if (isset($data['verifiedEmails']) && $data['verifiedEmails']) {
			if (!in_array($userdata['email'], $data['verifiedEmails']))
				list($userdata['email']) = array_values($data['verifiedEmails']);
			$userdata['verifiedEmail'] = true;
		} else {
			/*
			 * Check for conflict:
			 */
			if ($major >= 7)
				$account = user_load_by_mail($userdata['email']);
			else
				$account = user_load(array(
					'mail' => $userdata['email']
				));
			if ($account && isset($account->uid)) {
				if (variable_get('avoid_user_register_form_by_reg_without_valid_email', FALSE)) {
					/*
					 * Email conflict... trying to hack around it.
					 */
					$userdata['autogen'] = array('name' => $userdata['name'], 'email' => $userdata['email']);
					$randstr = substr(sha1(mt_rand().mt_rand()), 0, 4);
					if ($userdata['displayName'])
						$userdata['name'] = $userdata['displayName'];
					else
						$userdata['name'] = 'inval'.$randstr;
					$userdata['email'] = 'inval'.$randstr.'@autoreg.invalid.ndla.no';
				} else
					$saveok = false;
			}
			if ($major >= 7)
				$account = user_load_by_name($userdata['name']);
			else
				$account = user_load(array(
					'name' => $userdata['name']
				));
			if ($account && isset($account->uid)) {
				if (variable_get('avoid_user_register_form_by_reg_without_valid_email', FALSE)) {
					/*
					 * Name conflict... trying to hack around it.
					 */
					if (!$userdata['autogen'])
						$userdata['autogen'] = array('name' => $userdata['name'], 'email' => $userdata['email']);
					$randstr = substr(sha1(mt_rand().mt_rand()), 0, 4);
					if ($userdata['firstName'])
						$userdata['name'] = $userdata['firstName'].' '.$randstr;
					else if ($userdata['displayName'])
						$userdata['name'] = $userdata['displayName'].' '.$randstr;
					else
						$userdata['name'] = 'inval'.$randstr;
				} else
					$saveok = false;
			}
		}
   		/* Register new user */
		if ($saveok) {
			$save = array(
				'name' => $userdata['name'],
				'mail' => $userdata['email'],
				'pass' => user_password(),
				'status' => 1
			);
			$account = new stdClass();
			$account->uid = '';
			$account = user_save($account, $save);
			if ($account && $account->uid) {
				user_set_authmaps($account, array(
					$identity.'_seriaauth' => $identity
				));
				if ($major >= 7) {
					$form_state['uid'] = $account->uid;
					user_login_submit(array(), $form_state);
				} else
					user_external_login($account);
				$_SESSION['authenticatedSiteId'] = $id;
				$_SESSION['seriaauth']['sessiondata'] = $seriaauthSessionData;
				self::setUserAuthValues($account->uid, $seriaauthSessionData);
				self::callCustomCode('loggedIn', array($account, true /* first login */));
				self::redirectTo($continue);
			}
		}
		if (variable_get('user_register', 1)) {
			if ($major >= 7)
				$form_state = form_state_defaults();
			$form_state['redirect'] = null;
			$form_state['values']['name'] = $userdata['name'];
			$form_state['values']['mail'] = $userdata['email'];
			$form_state['values']['pass']  = user_password();
			$form_state['values']['status'] = variable_get('user_register', 1) ? 1 : 0;
			$form_state['values']['response'] = $data;
			$form_state['values']['identity'] = $identity;
			$form_state['values']['provider'] = $id;
			if (!$userdata['email'] || !$userdata['displayName']) {
				drupal_set_message(t('Please complete the registration by filling out the form below.'), 'warning');
				$success = false;
			} else if (!$userdata['verifiedEmail'] && !variable_get('autoregister_external_user_without_email_verify', FALSE) && variable_get('user_email_verification', TRUE)) {
				drupal_set_message(t('Please complete the registration by filling out the form below.'), 'warning');
				$success = false;
			} else {
				if ($major >= 7)
					$formGenerator = 'seriaauth_drupal7_user_register_form_without_admin_access';
				else
					$formGenerator = 'user_register';
				$form = drupal_retrieve_form($formGenerator, $form_state);
				if ($major >= 7) {
					foreach ($form['account'] as $name => &$element) {
						if (is_array($element))
							$element['#parents'] = array($name);
					}
					unset($element);
				}
				drupal_prepare_form($formGenerator, $form, $form_state);
				drupal_validate_form($formGenerator, $form, $form_state);
				$success = !form_get_errors();
				if (!$success) {
					drupal_set_message(t('Account registration using the information provided by your OpenID provider failed due to the reasons listed below. Please complete the registration by filling out the form below. If you already have an account, you can <a href="@login">log in</a> now and add your OpenID under "My account".', array('@login' => url('user/login'))), 'warning');
					// Append form validation errors below the above warning.
					$messages = drupal_get_messages('error');
					foreach ($messages['error'] as $message) {
						drupal_set_message( $message, 'error');
					}
				}
			}
			if (!$success) {
				if ($userdata['autogen']) {
					/* Undo autogen */
					$form_state['values']['name'] = $userdata['autogen']['name'];
					$form_state['values']['mail'] = $userdata['autogen']['email'];
				}
				$_SESSION['seriaauth']['values'] = $form_state['values'];
				$_SESSION['seriaauth']['data'] = $data;
				$_SESSION['seriaauth']['identity'] = $identity;
				$_SESSION['seriaauth']['params'] = $params;
				$_SESSION['seriaauth']['sessiondata'] = $seriaauthSessionData;
				$destination = drupal_get_destination();
				unset($_REQUEST['destination']);
				self::drupal_goto('user/register', $destination);
			} else {
				unset($form_state['values']['response']);
				$account = user_save('', $form_state['values']);
				if (!$account) {
					drupal_set_message(t("Error saving user account."), 'error');
					self::drupal_goto();
				}
				user_set_authmaps($account, array(
					$identity.'_seriaauth' => $identity
				));
				if ($major >= 7) {
					$form_state['uid'] = $account->uid;
					user_login_submit(array(), $form_state);
				} else
					user_external_login($account);
				$_SESSION['authenticatedSiteId'] = $id;
				$_SESSION['seriaauth']['sessiondata'] = $seriaauthSessionData;
				self::setUserAuthValues($account->uid, $seriaauthSessionData);
				self::callCustomCode('loggedIn', array($account, true /* first login */));
				self::redirectTo($continue);
			}
			drupal_redirect_form($form, $form_state['redirect']);
		} else {
			drupal_set_message(t('Only administrators can create new user accounts.'), 'error');
		}
	}
	public static function getAuthBaseUrl($id)
	{
		$sites = self::getStoredSites();
		if (isset($sites[$id])) {
			$params = $sites[$id];
			if (!isset($params['baseUrl'])) {
				$params['baseUrl'] = 'https://'.$params['host'];
				self::updateSite($params);
			}
			return $params['baseUrl'];
		}
		return null;
	}
	public static function menuAuthenticatedCall($id=false)
	{
		if (user_is_logged_in())
			self::drupal_goto($_GET['continue']);
		$sites = self::getStoredSites();
		if (!isset($sites[$id]))
			self::drupal_goto('');
		$params = $sites[$id];
		if ($_POST['loggedIn']) {
			$authBaseUrl = self::getAuthBaseUrl($id);
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $authBaseUrl.'/seria/api/?apiPath=SAPI_ExternalReq2/getUserSession');
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, array('requestToken' => $_POST['openSessionToken']));
			curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
			$data = curl_exec($ch);
			$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			if ($httpCode != 200) {
				$error = curl_error($ch);
			} else
				$error = '';
			curl_close($ch);
			if ($httpCode == 200) {
				$data = json_decode($data, true);
				$data = array('data' => $data);
				if (isset($data['data']['safeEmails']) && $data['data']['safeEmails']) {
					$data['verifiedEmails'] = $data['data']['safeEmails'];
					unset($data['data']['safeEmails']);
				}
				if (isset($_POST['roamAuthUrl']))
					$roamauth = $_POST['roamAuthUrl'];
				else
					$roamauth = null;
				self::authenticated($id, $params, $data, $_GET['continue'], $roamauth);
			}
		}
		self::drupal_goto('');
	}
	public static function menuAuthenticateCall($id=false)
	{
		/* Try to find referrer either by continue param or referrer */
		if (isset($_GET['continue']))
			$continue = $_GET['continue'];
		else {
			if (isset($_SERVER['HTTP_REFERER'])) {
				/*
				 * Sanitizing required!
				 */
				$url = $_SERVER['HTTP_REFERER'];
				if (parse_url($url, PHP_URL_HOST) == $_SERVER['SERVER_NAME'])
					$continue = $url;
				else
					$continue = url('', array('absolute' => true));
			} else
				$continue = url('', array('absolute' => true));
		}
		if (user_is_logged_in())
			self::drupal_goto($continue);
		$sites = self::getEnabledSites();
		if ($id === false && count($sites) == 1)
			$id = array_shift(array_keys($sites));
		if ($id !== false) {
			$params = $sites[$id];
			?><!DOCTYPE html>
				<form method='post' action="<?php echo htmlspecialchars(self::getAuthBaseUrl($id).'/seria/components/Authproviders/pages/externalReq2.php?interactive=1&guest=1&auth_abort='.htmlspecialchars(url('', array('absolute' => true)))); ?>">
					<input type='hidden' name='returnUrl' value="<?php echo htmlspecialchars(url('seriaauth/authenticated/'.$id, array('query' => array('continue' => $continue), 'absolute' => true))); ?>" />
					<div>
						<input id='submitb' type='submit' value='Continue' />
						<span id='msg'></span>
					</div>
				</form>
				<script type='text/javascript'>
					<!--
						document.getElementById('submitb').style.display = 'none';
						document.getElementById('msg').innerHTML = 'Please wait...';
						document.getElementById('submitb').form.submit();
					-->
				</script>
				<?php
			die();
		}
		ob_start();
		foreach ($sites as $id => $params)
			if (!$params['enabled'])
				continue;
			echo l(
				t('Log in using @name', array('%host' => $params['host'], '@name' => $params['name'])),
				'seriaauth/authenticate/'.$id,
				array(
					'attributes' => array(
						'title' => t('Login using an external provider, %name (%host)', array('%host' => $params['host'], '%name' => $params['name']))
					),
					'query' => drupal_get_destination()
				)
			);
		return ob_get_clean();
	}
	public static function logoutRedirected($id=false)
	{
		self::externalLogout($id);
		return '';
	}
	public static function externalLogout($id=false)
	{
		if ($id !== false || (isset($_SESSION['authenticatedSiteId']))) {
			/*
			 * Logging out by redirecting to remote.
			 */
			$sites = self::getStoredSites();
			if (!$id)
				$id = $_SESSION['authenticatedSiteId'];
			if (!isset($sites[$id]))
				return;
			$params = $sites[$id];
			unset($_SESSION['authenticatedSiteId']);

			/* Force logout */
			global $user;
			$tmp = NULL;
			session_destroy();
			user_module_invoke('logout', $tmp, $user);
			$user = drupal_anonymous_user();

			$logoutUrl = self::getAuthBaseUrl($id).'/seria/components/Authproviders/pages/externalLogout.php?continue='.url('', array('absolute' => true));
			self::drupal_goto($logoutUrl);
		}
	}
	public static function userOperation($op, &$edit, &$account, $category = NULL)
	{
		if ($op == 'insert' && isset($_SESSION['seriaauth']) && isset($_SESSION['seriaauth']['identity'])) {
			$params = $_SESSION['seriaauth']['params'];
			$identity = $_SESSION['seriaauth']['identity'];
			drupal_set_message(t('Once you have verified your email address, you may log in via %name (%host).', array('%host' => $params['host'], '%name' => $params['name'])));
			user_set_authmaps($account, array(
				$identity.'_seriaauth' => $identity
			));
			unset($_SESSION['seriaauth']);
		} else if ($op == 'logout') {
			if (isset($_SESSION['authenticatedSiteId']))
				self::drupal_goto('seriaauth/logout/'.$_SESSION['authenticatedSiteId']);
		}
	}

	public static function admin()
	{
		$sites = self::getStoredSites();
		$site = null;
		if (isset($_GET['host'])) {
			foreach ($sites as $wb) {
				if ($wb['host'] == $_GET['host']) {
					$site = $wb;
					break;
				}
			}
		}
		$form = array();
		if ($site === null) {
		$sites = self::getStoredSites();
			if (!isset($_GET['addHost'])) {
				foreach ($sites as $site) {
					$form['host'.sha1($site['host'])] = array(
						'#type' => 'checkbox',
						'#title' => l($site['name'], 'admin/settings/seriaauth', array(
							'query' => array(
								'host' => $site['host']
							)
						)),
						'#value' => ($site['enabled'] ? 1 : 0)
					);
				}
				$form['buttons'] = array(
					'save' => array(
						'#type' => 'button',
						'#value' => t('Save'),
						'#weight' => 19,
						'#executes_submit_callback' => TRUE,
						'#submit' => array('seriaauth_admin_save')
					),
					'addHost' => array(
						'#type' => 'button',
						'#value' => t('Add remote site'),
						'#weight' => 19,
						'#executes_submit_callback' => TRUE,
						'#submit' => array('seriaauth_admin_host_add')
					),
					'cancel' => array(
						'#type' => 'button',
						'#value' => t('Cancel'),
						'#weight' => 19,
						'#executes_submit_callback' => TRUE,
						'#submit' => array('seriaauth_admin_cancel')
					)
				);
			} else {
				$form['name'] = array(
					'#type' => 'textfield',
					'#title' => t('Name'),
					'#default_value' => '',
					'#required' => TRUE
				);
				$form['host'] = array(
					'#type' => 'textfield',
					'#title' => t('Hostname'),
					'#default_value' => '',
					'#required' => TRUE
				);
				$form['baseUrl'] = array(
					'#type' => 'textfield',
					'#title' => t('Base URL'),
					'#default_value' => 'https://example.com',
					'#required' => TRUE
				);
				$form['buttons'] = array(
					'save' => array(
						'#type' => 'button',
						'#value' => t('Save'),
						'#executes_submit_callback' => TRUE,
						'#submit' => array('seriaauth_admin_host_submit')
					),
					'cancel' => array(
						'#type' => 'button',
						'#value' => t('Cancel'),
						'#executes_submit_callback' => TRUE,
						'#submit' => array('seriaauth_admin_host_cancel')
					)
				);
			}
		} else {
			$form['name'] = array(
				'#type' => 'textfield',
				'#title' => t('Name'),
				'#default_value' => $site['name'],
				'#required' => TRUE
			);
			$form['baseUrl'] = array(
					'#type' => 'textfield',
					'#title' => t('Base URL'),
					'#default_value' => $site['baseUrl'],
					'#required' => TRUE
			);
			$form['buttons'] = array(
				'save' => array(
					'#type' => 'button',
					'#value' => t('Save'),
					'#executes_submit_callback' => TRUE,
					'#submit' => array('seriaauth_admin_host_submit')
				),
				'cancel' => array(
					'#type' => 'button',
					'#value' => t('Cancel'),
					'#executes_submit_callback' => TRUE,
					'#submit' => array('seriaauth_admin_host_cancel')
				)
			);
		}
		return $form;
	}
	public static function adminHostSubmit($form_id, $form_values)
	{
		if ($_GET['host']) {
			if (!isset($form_values['values']['host']))
				self::updateSite(array(
					'host' => $_GET['host'],
					'name' => $form_values['values']['name'],
					'baseUrl' => $form_values['values']['baseUrl'],
				));
		} else {
			self::addSite(array(
				'host' => $form_values['values']['host'],
				'name' => $form_values['values']['name'],
				'baseUrl' => $form_values['values']['baseUrl'],
				'enabled' => false
			));
		}
		self::drupal_goto('admin/settings/seriaauth');
	}
	public static function adminHostCancel($form_id, $form_values)
	{
		self::drupal_goto('admin/settings/seriaauth');
	}
	public static function adminSave($form_id, $form_values)
	{
		$major = self::getDrupalMajorVersion();
		$sites = self::getStoredSites();
		$hash = sha1(serialize($sites));
		if ($major >= 7) {
			foreach ($sites as &$site)
				$site['enabled'] = isset($form_values['input']['host'.sha1($site['host'])]);
		} else {
			foreach ($sites as &$site)
				$site['enabled'] = isset($form_values['clicked_button']['#post']['host'.sha1($site['host'])]);
		}
		if ($hash != sha1(serialize($sites)))
			variable_set('seriaauth_sites', $sites);
		self::drupal_goto('');
	}

	public static function roamauthParseDocument($xmldoc)
	{
		$userdata = null;
		$error = false;
		try {
			$xmlerr = libxml_use_internal_errors();
			$xml = simplexml_load_string($xmldoc);
			if ($xml) {
				$userdata = array();
				$userdata['extensionValues'] = array();
				foreach ($xml as $xmlobj) {
					$name = $xmlobj->getName();
					if ($name == 'extensionValues') {
						$extv = $xmlobj->children();
						foreach ($extv as $extvobj) {
							$name = $extvobj->getName();
							$value = "".$extvobj;
							$userdata['extensionValues'][$name] = $value;
						}
						continue;
					}
					$value = "".$xmlobj;
					if (trim($value) != '') {
						if (!isset($userdata[$name]))
							$userdata[$name] = $value;
						else if (is_array($userdata[$name]))
							$userdata[$name][] = $value;
						else
							$userdata[$name] = array($userdata[$name], $value);
					}
				}
			}
		} catch (Exception $e) {
			$error = $e;
		}
		libxml_use_internal_errors($xmlerr);
		if ($error)
			throw $error;
		return $userdata;
	}

	public static function roamauthValidateDocument($host, $xmldoc)
	{
		$userdata = self::roamauthParseDocument($xmldoc);
		if ($userdata) {
			if (isset($userdata['hostname']) && $userdata['hostname'] == $host) {
				return $userdata;
			} else
				throw new Exception('Hostname-field in XML is invalid (must be equal to host in url).');
		} else
			throw new Exception('XML error(s)');
	}

	/**
	 * Authenticate by an xml-document. This can be used for
	 * authenticating RPCs.
	 *
	 * @param unknown_type $xmldocurl
	 */
	public static function roamauthParse($xmldocurl)
	{
		$host = parse_url($xmldocurl, PHP_URL_HOST);
		$xmldoc = file_get_contents($xmldocurl);
		if (!$xmldoc)
			throw new Exception('Not logged in or missing XML-doc!');
		return self::roamauthValidateDocument($host, $xmldoc);
	}
	public static function roamauthResolveUserByDocument($authServerHost, $xmlContent)
	{
		$userdata = self::roamauthValidateDocument($authServerHost, $xmlContent);
		if (!$userdata)
			return null;

		$params = null;
		$sites = self::getStoredSites();
		foreach ($sites as $id => $site) {
			if ($site['host'] == $authServerHost && $site['enabled']) {
				$siteid = $id;
				$params = $site;
				break;
			}
		}
		if ($params === null)
			throw new Exception('The site '.$authServerHost.' is not trusted to authenticate users.');

		/* Map the values into the data array */
		$fields = array_merge(
			$userdata['extensionValues'],
			array(
				'uid' => $userdata['uid'],
				"firstName" => $userdata['firstName'],
				"lastName" => $userdata['lastName'],
				"displayName" => $userdata['displayName'],
				"username" => $userdata['username'],
				"email" => $userdata['email'],
				'is_administrator' => false,
				'guestAccount' => true
			)
		);
		$verified = array();
		if ($userdata['verifiedEmail']) {
			if (is_string($userdata['verifiedEmail']))
				$verified = array($userdata['verifiedEmail']);
			else if (is_array($userdata['verifiedEmail']))
				$verified = $userdata['verifiedEmail'];
		}
		$data = array('data' => $fields);
		if ($verified)
			$data['verifiedEmails'] = $verified;

		return self::getExistingUser($siteid, $params, $data);
	}

	/**
	 * Authenticate by an xml-document. This can be used for
	 * authenticating RPCs.
	 *
	 * @param unknown_type $xmldocurl
	 */
	public static function roamauth($xmldocurl, $returnData=false)
	{
		$userdata = self::roamauthParse($xmldocurl);

		/* Find the host parameters */
		$host = parse_url($xmldocurl, PHP_URL_HOST);
		$params = null;
		$sites = self::getStoredSites();
		foreach ($sites as $id => $site) {
			if ($site['host'] == $host && $site['enabled']) {
				$siteid = $id;
				$params = $site;
				break;
			}
		}
		if ($params === null)
			throw new Exception('The site '.$host.' is not trusted to authenticate users.');

		/* Map the values into the data array */
		$fields = array_merge(
			$userdata['extensionValues'],
			array(
				'uid' => $userdata['uid'],
				"firstName" => $userdata['firstName'],
				"lastName" => $userdata['lastName'],
				"displayName" => $userdata['displayName'],
				"username" => $userdata['username'],
				"email" => $userdata['email'],
				'is_administrator' => false,
				'guestAccount' => true
			)
		);
		$verified = array();
		if ($userdata['verifiedEmail']) {
			if (is_string($userdata['verifiedEmail']))
				$verified = array($userdata['verifiedEmail']);
			else if (is_array($userdata['verifiedEmail']))
				$verified = $userdata['verifiedEmail'];
		}
		$data = array('data' => $fields);
		if ($verified)
			$data['verifiedEmails'] = $verified;
		$res = self::rpcAuthenticated($siteid, $params, $data, $xmldocurl);
		
		if ($res && $returnData)
			$res = $data;
		return $res;
	}
	public static function validateSsoLogin($drupalUser)
	{
		if (!isset($_SESSION['seriaauth']) || !isset($_SESSION['seriaauth']['sessiondata']) || !isset($_SESSION['seriaauth']['sessiondata']['roamauth']))
			return true; /* Not logged in by sso, and not our business */
		if (isset($_SESSION['seriaauth']['sessiondata']['validatedAt'])) {
			$expiry = $_SESSION['seriaauth']['sessiondata']['validatedAt'] + 60;
			if ($expiry >= time()) {
				return true; /* Ok - for a while */
			}
		}
		$xmldocurl = $_SESSION['seriaauth']['sessiondata']['roamauth'];
		if (!$xmldocurl)
			return true; /* Not logged in by sso, and not our business */
		try {
			$host = parse_url($xmldocurl, PHP_URL_HOST);
			$xmldoc = file_get_contents($xmldocurl);
			if (!$xmldoc)
				throw new Exception('Not logged in or missing XML-doc!');
			$validUser = self::roamauthResolveUserByDocument($host, $xmldoc);
			if ($validUser['login'] && isset($validUser['user']))
				$validUser = $validUser['user'];
			else
				return false;
			if (!$validUser || !$validUser->uid)
				return false;
			if ($validUser->uid == $drupalUser->uid)
				return true;
			else
				return false;
		} catch (Exception $e) {
			/* Unsure about the login, kick the user out! */
			return false;
		}
	}

	public static function getAuthValue($name=NULL)
	{
		if ($name !== NULL) {
			if (isset($_SESSION['seriaauth']) && isset($_SESSION['seriaauth']['sessiondata']) && isset($_SESSION['seriaauth']['sessiondata'][$name]))
				return $_SESSION['seriaauth']['sessiondata'][$name];
			else
				return NULL;
		} else {
			if (isset($_SESSION['seriaauth']) && isset($_SESSION['seriaauth']['sessiondata']))
				return $_SESSION['seriaauth']['sessiondata'];
			else
				return NULL;
		}
	}

	public static function deleteMe($taskUrl)
	{
		$taskData = file_get_contents($taskUrl);
		if ($taskData)
			$taskData = json_decode($taskData, true);
		$deleteUser = false;
		if ($taskData) {
			if ($taskData['userxml']) {
				$host = parse_url($taskUrl, PHP_URL_HOST);
				$lookup = self::roamauthResolveUserByDocument($host, $taskData['userxml']);
				if ($lookup && $lookup['user']) {
					$deleteUser = $lookup['user'];
					$major = self::getDrupalMajorVersion();
					if ($major >= 7)
						user_delete_multiple(array($deleteUser->uid));
					else
						user_delete(array(), $deleteUser->uid);
				} /* If the user does not exist, just report back deleted */
				/* Report done */
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $taskData['postReportDoneUrl']);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
				curl_setopt($ch, CURLOPT_POST, true);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $taskData['postReportDoneData']);
				curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
				$data = curl_exec($ch);
				$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
				if ($httpCode != 200) {
					$error = curl_error($ch);
				} else
					$error = '';
				curl_close($ch);
				if ($httpCode == 200) {
					$data = json_decode($data, true);
				}
				if ($error)
					throw new Exception('Failed to report back that the user was deleted!');
				return true;
			}
		}
		return false;
	}
}
