<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
  <head>
    <script src="https://www.wiris.net/demo/editor/editor"></script>
    <?php print $scripts; ?>
  </head>
  <body>
    <div id="editorContainer"></div>
    <div style="margin-top: 3px">
      <input type="button" id="insert" value="" />
      <input type="button" id="cancel" value="<?php print t('Cancel'); ?>" />
    </div>
  </body>
</html>
