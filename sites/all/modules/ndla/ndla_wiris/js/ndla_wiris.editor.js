(function ($) {
  $(document).ready(function () {

    var mathml = tinyMCEPopup.getWindowArg('mathml');
    var editor;
    editor = com.wiris.jsEditor.JsEditor.newInstance({'language': 'en'});
    editor.insertInto(document.getElementById('editorContainer'));

    var editExisting = false;
    if (mathml) {
      editExisting = true;
      $('#insert').val(Drupal.t('Update'));
      editor.setMathML(mathml);
    } else {
      $('#insert').val(Drupal.t('Insert'));
    }

    $('#insert').click(function() {
      var mathml = editor.getMathML();

      if (editExisting) {
        var node = $(tinyMCEPopup.editor.selection.getNode());
        if (!node.is('span.Wirisformula')) {
          node = node.parents('span.Wirisformula');
        }
        if (node.length == 1) {
          // Select the <math> node, and replace it with new content
          mathNode = node.children().get(0);
          tinyMCEPopup.editor.execCommand('mceSelectNode', false, mathNode);
          tinyMCEPopup.editor.execCommand('mceReplaceContent', false, '<span class="Wirisformula">' + mathml + '</span>&nbsp;');
          tinyMCEPopup.editor.selection.getSel().collapseToEnd();
        }
        tinyMCEPopup.close();
        return false;
      }
      tinyMCEPopup.editor.execCommand('mceInsertContent', false, '<span class="Wirisformula">' + mathml + '</span>&nbsp;');
      tinyMCEPopup.close();
      return false;
    });

    $('#cancel').click(function() {
      tinyMCEPopup.close();
      return false;
    });
  });
})(jQuery);
