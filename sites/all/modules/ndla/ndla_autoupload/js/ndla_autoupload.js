/*
 * Behavior for the automatic file upload
 */
(function ($) {
  Drupal.behaviors.ndlaAutoUpload = {
    attach: function(context, settings) {
      var uploading = false;
      $('.form-type-dragndrop-upload').bind('DOMSubtreeModified',function(){
        if(uploading || $('.droppable-controls input:visible').length == 0) {
          return; 
        }
        uploading = true;
        $('.droppable-controls input:visible').mousedown();
        setTimeout(function(){
          uploading = false;
        }, 500);
      });
    }
  };
})(jQuery);