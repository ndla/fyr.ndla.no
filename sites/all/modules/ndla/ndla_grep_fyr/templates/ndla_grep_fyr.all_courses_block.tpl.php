<ul id='all-courses-list'>
  <?php foreach($common as $node): ?>
  <li class='common-course'>
    <div class='common-course-header'>
    <?php print l($node['icon'] . '<span>' . $node['title'] . '</span>', 'node/' . $node['nid'], array('html' => true)); ?>
    </div>
    <ul>
      <?php foreach($courses as $course): ?>
        <li class='course'>
          <?php if(!empty($course['nid'])): ?>
            <a href="<?php print $GLOBALS['base_path']; ?>search?f[0]=field_courses%3A<?php print $course['nid']; ?>&f[1]=field_common_courses%3A<?php print $node['nid']; ?>"><?php print $course['title']; ?></a>
          <?php else: ?>
            <a href="<?php print $GLOBALS['base_path']; ?>search?f[0]=field_common_courses%3A<?php print $node['nid']; ?>&f[1]=field_courses:!"><?php print $course['title']; ?></a>
          <?php endif; ?>
        </li>
      <?php endforeach ?>
    </ul>
  </li>
  <?php endforeach ?>
</ul>