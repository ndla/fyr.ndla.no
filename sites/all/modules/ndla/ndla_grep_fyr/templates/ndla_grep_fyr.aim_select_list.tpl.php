<ul <?php if($hidden) print 'class="hidden"' ?>>
  <?php foreach($items as $item): ?>
    <?php
      $json_tree = $tree;
      if(!empty($item['extra_id'])) {
        if(!is_array($item['extra_id'])) {
          $item['extra_id'] = array($item['extra_id']);
        }
        foreach($item['extra_id'] as $id) {
          $json_tree[] = array(
            '_id' => $id,
          );
        }
      }
      $json_tree[] = array(
        '_name' => $item['name'],
        '_id' => $item['id'],
      );
      $json_tree = json_encode($json_tree);
    ?>
  <li>
    <span class='aim-list-item' data-endpoint='<?php print $item['endpoint'] ?>' data-target='<?php print $target ?>' data-callback='<?php print $callback ?>' data-tree='<?php print $json_tree ?>'><?php print $item['name'] ?><?php if($target == 'left') print "<i class='fa fa-angle-right'></i></span>" ?>
    <?php if(!empty($item['children'])): ?>
      <?php print $item['children'] ?>
    <?php endif ?>
  </li>
<?php endforeach ?>
</ul>