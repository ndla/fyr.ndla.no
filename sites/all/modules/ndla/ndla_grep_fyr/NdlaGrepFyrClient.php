<?php

class NdlaGrepFyrClient {

  private static function _post($path, $data) {
    global $ndla_migrate_running;
    if(isset($ndla_migrate_running) && $ndla_migrate_running == TRUE) {
      return FALSE; // Return if we are migrating woth ndla_migrate module
    }
    $url = self::_build_url($path);
    $response = drupal_http_request($url, array('headers' => array('Accept' => 'application/json', 'Content-Type' => 'application/json'), 'method' => 'POST', 'data' => json_encode($data)));
    if(!in_array($response->code, array(200, 201))) {
      return FALSE;
    } else {
      return TRUE;
    }
  }

  private static function _delete($path, $data) {
    global $ndla_migrate_running;
    if(isset($ndla_migrate_running) && $ndla_migrate_running == TRUE) {
      return FALSE; // Return if we are migrating woth ndla_migrate module
    }
    $url = self::_build_url($path);
    $response = drupal_http_request($url, array('headers' => array('Accept' => 'application/json', 'Content-Type' => 'application/json'), 'method' => 'DELETE', 'data' => json_encode($data)));
    if(!in_array($response->code, array(200, 201))) {
      return FALSE;
    } else {
      return TRUE;
    }
  }

  private static function _get($path, $cache = TRUE) {
    $wakeup = variable_get('ndla_grep_wakeup', 5) * 3600;
    $limit = rand(0, $wakeup) + variable_get('ndla_grep_sleep', 0);
    if($limit > time()) {
      drupal_set_message(t('MyCurriculum is not responding.'), 'error');
      return FALSE;
    }
    $url = self::_build_url($path);
    if($cache) {
      $data = cache_get($url, 'cache_ndla_grep');
      if(!empty($data->data)) {
        return $data->data;
      }
    }
    $response = drupal_http_request($url, array('headers' => array('Accept' => 'application/json'), 'timeout' => variable_get('ndla_grep_timeout', 30)));
    if(in_array($response->code, array('-1'))) {
      variable_set('ndla_grep_sleep', time());
      drupal_set_message(t('MyCurriculum is not responding.'), 'error');
      return FALSE;
    }
    if($response->code != 200) {
      return FALSE;
    }
    if(variable_get('ndla_grep_sleep', 0) != 0) {
      variable_set('ndla_grep_sleep', 0);
    }
    $data = json_decode($response->data);
    if($cache) {
      cache_set($url, $data, 'cache_ndla_grep');
    }
    return $data;
  }

  private static function _build_url($path) {
    if(preg_match('/^http/', $path)) {
      return $path;
    }
    $host = variable_get('ndla_grep_host', '');
    $account = variable_get('ndla_grep_account', '');
    $version = variable_get('ndla_grep_version', '');
    $ssl = variable_get('ndla_grep_ssl', FALSE);
    $username = variable_get('ndla_grep_username', '');
    $password = variable_get('ndla_grep_password', '');
    $url = 'http://';
    if($ssl) {
      $url = 'https://';
    }
    if(!empty($username) || !empty($password)) {
      $url .= $username . ':' . $password . '@';
    }
    $url .= $host . '/v1/users/' . $account . '/' . $path;
    if(!empty($version)) {
      if(strpos($url, '?') !== FALSE) {
        $url .= '&';
      } else {
        $url .= '?';
      }
      $url .= $version;
    }
    return $url;
  }

  private static function _extract_name($names) {
    global $language;
    $psi = NdlaGrepLanguages::iso639_1_psi($language->language);
    $default = '';
    foreach($names as $name) {
      if(empty($default)) {
        $default = $name->name;
      }
      if(in_array($psi, $name->scopes)) {
        return $name->name;
      }
      if($name->isLanguageNeutral) {
        $default = $name->name;
      }
    }
    return $default;
  }

  public static function save_resource($node) {
    $author = user_load($node->uid);
    $resource = array(
      'psi' => self::get_psi('node/' . $node->nid),
      'status' => 'TRUE',
      'names' => array(
        array(
          'name' => $node->title,
          'languageCode' => NdlaGrepLanguages::iso639_1_iso639_2($node->language),
          'isLanguageNeutral' => ($node->language == 'und'),
        ),
      ),
      'ingress' => (object)array(
        'text' => '',
        'languageCode' => NdlaGrepLanguages::iso639_1_iso639_2($node->language),
        'isLanguageNeutral' => ($node->language == 'und'),
      ),
      'type' => 'ndla-node',
      'licenses' => array(),
      'authors' => array(
        array(
          'name' => !empty($author->name) ? $author->name : '',
          'url' => self::get_psi('user/' . $author->uid),
        )
      ),
    );
    if(!empty($node->field_license[LANGUAGE_NONE][0]['value'])) {
      $resource['licenses'][] = array(
        'name' => $node->field_license[LANGUAGE_NONE][0]['value'],
        'scopes' => array(
          'http://psi.mycurriculum.org/#universal-type',
          'http://psi.oasis-open.org/iso/639/#eng',
        ),
        'isLanguageNeutral' => TRUE,
      );
    }
    return self::_post('resources', $resource);
  }

  public static function load_aims($node) {
    $data = self::_get('resources?psi=' . self::get_psi('node/' . $node->nid) . '&competence-aims=true');
    if($data == FALSE) {
      self::save_resource($node);
    }
    $aims = array(
      'courses' => array(),
      'common_courses' => array(),
    );
    if(empty($data->resource->relations)) {
      return $aims;
    }
    foreach($data->resource->relations as $aim) {
      if($aim->courseType == 'EDUCATIONAL_PROGRAM') {
        /* Courses */
        $set_response = self::_get('competence-aim-sets/' . $aim->competenceAimSetId);
        $curriculum_response = self::_get($set_response->competenceAimSet->links->parent);
        $structure_response = self::_get($aim->curriculumSet->links->parents[0]);
        $course_response = self::_get('courses/' . $aim->courseId);
        /* Courses */
        if(empty($aims['courses'][$aim->courseId])) {
          $aims['courses'][$aim->courseId] = array(
            '_name' => self::_extract_name($course_response->course->courseStructures[0]->courseStructure->names),
            '_id' => $aim->courseId,
          );
        }
        /* Course structure */
        if(empty($aims['courses'][$aim->courseId][$aim->level->id])) {
          $aims['courses'][$aim->courseId][$aim->level->id] = array(
            '_name' => self::_extract_name($aim->level->names),
            '_id' => $aim->level->id,
          );
        }
        if(empty($aims['courses'][$aim->courseId][$aim->level->id][$structure_response->courseStructure->id])) {
          $aims['courses'][$aim->courseId][$aim->level->id][$structure_response->courseStructure->id] = array(
            '_name' => self::_extract_name($structure_response->courseStructure->names),
            '_id' => $structure_response->courseStructure->id,
          );
        }
        /* Curiculum set */
        if(empty($aims['courses'][$aim->courseId][$aim->level->id][$structure_response->courseStructure->id][$aim->curriculumSet->id])) {
          $aims['courses'][$aim->courseId][$aim->level->id][$structure_response->courseStructure->id][$aim->curriculumSet->id] = array(
            '_id' => $aim->curriculumSet->id,
          );
        }
        /* Curriculum */
        if(empty($aims['courses'][$aim->courseId][$aim->level->id][$structure_response->courseStructure->id][$aim->curriculumSet->id][$curriculum_response->curriculum->id])) {
          $aims['courses'][$aim->courseId][$aim->level->id][$structure_response->courseStructure->id][$aim->curriculumSet->id][$curriculum_response->curriculum->id] = array(
            '_name' => self::_extract_name($curriculum_response->curriculum->names),
            '_id' => $curriculum_response->curriculum->id,
          );
        }
        /* Competence aim set */
        if(empty($aims['courses'][$aim->courseId][$aim->level->id][$structure_response->courseStructure->id][$aim->curriculumSet->id][$curriculum_response->curriculum->id][$set_response->competenceAimSet->id])) {
          $aims['courses'][$aim->courseId][$aim->level->id][$structure_response->courseStructure->id][$aim->curriculumSet->id][$curriculum_response->curriculum->id][$set_response->competenceAimSet->id] = array(
            '_name' => self::_extract_name($set_response->competenceAimSet->names),
            '_id' => $set_response->competenceAimSet->id,
          );
        }
        /* Competence aim */
        if(empty($aims['courses'][$aim->courseId][$aim->level->id][$structure_response->courseStructure->id][$aim->curriculumSet->id][$curriculum_response->curriculum->id][$set_response->competenceAimSet->id][$aim->competenceAim->id])) {
          $aims['courses'][$aim->courseId][$aim->level->id][$structure_response->courseStructure->id][$aim->curriculumSet->id][$curriculum_response->curriculum->id][$set_response->competenceAimSet->id][$aim->competenceAim->id] = array(
            '_name' => self::_extract_name($aim->competenceAim->names),
            '_id' => $aim->competenceAim->id,
          );
        }
      } elseif($aim->courseType == 'CURRICULUM') {
        /* Common */
        $set_response = self::_get('competence-aim-sets/' . $aim->competenceAimSetId);
        $level_response = self::_get($set_response->competenceAimSet->links->parent);
        $course_response = self::_get('courses/' . $aim->courseId);
        /* Courses */
        if(empty($aims['common_courses'][$aim->courseId])) {
          $aims['common_courses'][$aim->courseId] = array(
            '_id' => $aim->courseId,
          );
        }
        /* Curiculum set */
        if(empty($aims['common_courses'][$aim->courseId][$aim->curriculumSet->id])) {
          $aims['common_courses'][$aim->courseId][$aim->curriculumSet->id] = array(
            '_id' => $aim->curriculumSet->id,
          );
        }
        /* Level and curriculum */
        if(empty($aims['common_courses'][$aim->courseId][$aim->curriculumSet->id][$level_response->curriculum->id])) {
          $aims['common_courses'][$aim->courseId][$aim->curriculumSet->id][$level_response->curriculum->id] = array(
            '_name' => self::_extract_name($level_response->curriculum->names),
            '_id' => $level_response->curriculum->id,
          );
        }
        if(empty($aims['common_courses'][$aim->courseId][$aim->curriculumSet->id][$level_response->curriculum->id][$aim->level->id])) {
          $aims['common_courses'][$aim->courseId][$aim->curriculumSet->id][$level_response->curriculum->id][$aim->level->id] = array(
            '_id' => $aim->level->id,
          );
        }
        $set_id = $set_response->competenceAimSet->id;
        foreach($level_response->curriculum->competenceAimSets as $set) {
          foreach($set->competenceAimSets as $set2) {
            if($set_id == $set2->id) {
              if(empty($aims['common_courses'][$aim->courseId][$aim->curriculumSet->id][$level_response->curriculum->id][$aim->level->id][$set->id])) {
                $aims['common_courses'][$aim->courseId][$aim->curriculumSet->id][$level_response->curriculum->id][$aim->level->id][$set->id] = array(
                  '_name' => self::_extract_name($set->names),
                  '_id' => $set->id,
                );
              }
              break(2);
            }
          }
        }
        /* Competence aim set */
        if(empty($aims['common_courses'][$aim->courseId][$aim->curriculumSet->id][$level_response->curriculum->id][$aim->level->id][$set->id][$set_response->competenceAimSet->id])) {
          $aims['common_courses'][$aim->courseId][$aim->curriculumSet->id][$level_response->curriculum->id][$aim->level->id][$set->id][$set_response->competenceAimSet->id] = array(
            '_name' => self::_extract_name($set_response->competenceAimSet->names),
            '_id' => $set_response->competenceAimSet->id,
          );
        }
        /* Competence aim */
        if(empty($aims['common_courses'][$aim->courseId][$aim->curriculumSet->id][$level_response->curriculum->id][$aim->level->id][$set->id][$set_response->competenceAimSet->id][$aim->competenceAim->id])) {
          $aims['common_courses'][$aim->courseId][$aim->curriculumSet->id][$level_response->curriculum->id][$aim->level->id][$set->id][$set_response->competenceAimSet->id][$aim->competenceAim->id] = array(
            '_name' => self::_extract_name($aim->competenceAim->names),
            '_id' => $aim->competenceAim->id,
          );
        }
      }
    }
    return $aims;
  }

  public static function getAimNames($id) {
    $data = self::_get("competence-aims/$id");
    return self::_extract_name($data->competenceAim->names);
  }

  public static function load_aims_with_names($node, $all_names = FALSE) {
    $data = self::_get('resources?psi=' . self::get_psi('node/' . $node->nid) . '&competence-aims=true');
    if($data == FALSE) {
      self::save_resource($node);
    }
    $aims = array();
    if(empty($data->resource->relations)) {
      return $aims;
    }
    foreach($data->resource->relations as $aim) {
      //Dont filter out names based on current language.
      if($all_names) {
        foreach($aim->competenceAim->names as $name) {
          $aims[$aim->competenceAim->id][] = $name->name;
        }
      }
      else {
        $aims[$aim->competenceAim->id] = self::_extract_name($aim->competenceAim->names);
      }
    }
    return $aims;
  }


  public static function load_aims_and_levels($node) {
    $data = self::_get('resources?psi=' . self::get_psi('node/' . $node->nid) . '&competence-aims=true');
    if($data == FALSE) {
      self::save_resource($node);
    }
    $aims = array();
    $levels = array();
    $courses = array();
    $common_courses = array();
    if(!empty($data->resource->relations)) {
      foreach($data->resource->relations as $aim) {
        if($aim->courseType == 'EDUCATIONAL_PROGRAM') {
          $courses[$aim->courseId] = $aim->courseId;
        }
        if($aim->courseType == 'CURRICULUM') {
          $common_courses[$aim->courseId] = $aim->courseId;
        }
        $aims[$aim->competenceAim->id] = self::_extract_name($aim->competenceAim->names);
        $levels[$aim->level->id] = self::_extract_name($aim->level->names);
      }
    }
    return array($aims, $levels, $courses, $common_courses);
  }

  public static function save_aims($node, $aims) {
    global $user;
    $data = array(
      'author' => array(
        'name' => $user->name,
        'url' => self::get_psi('user/' . $user->uid),
      ),
      'subjectMatterId' => '',
      'relations' => array(),
    );
    foreach($aims as $aim) {
      $data['relations'][] = array(
        'competenceAimId' => $aim['id'],
        'relationType' => 'related',
        'resourcePsi' => self::get_psi('node/' . $node->nid),
        'apprenticeRelevance' => FALSE,
        'curriculumSetId' => $aim['curriculum_set_id'],
        'level' => $aim['level'],
        'courseId' => $aim['course_id'],
        'courseType' => $aim['course_type'],
        'curriculumId' => $aim['curriculum_id'],
        'competenceAimSetId' => $aim['competence_aim_set_id'],
      );
    }
    if(!empty($data['relations'])) {
      $result = self::_post('relations', $data);
      self::_unset_cache($node->nid);
      return $result;
    }
    return TRUE;
  }

  public static function delete_aims($node) {
    $response = self::_get('resources?psi=' . self::get_psi('node/' . $node->nid) . '&competence-aims=true');
    if($response == FALSE) {
      self::save_resource($node);
    }
    $psi = self::get_psi('node/' . $node->nid);
    $data = array('relations' => array());
    if(!empty($response->resource->relations)) {
      foreach($response->resource->relations as $aim) {
        $data['relations'][] = array(
          'resourcePsi' => $psi,
          'humanId' => $aim->humanId,
          'competenceAimId' => $aim->competenceAim->id,
        );
      }
    }
    if(!empty($data)) {
      $result = self::_delete('relations', $data);
      self::_unset_cache($node->nid);
      return $result;
    }
    return TRUE;
  }

  private static function _unset_cache($nid) {
    $psi = self::get_psi('node/' . $nid);
    $url = self::_build_url('resources?psi=' . self::get_psi('node/' . $nid) . '&competence-aims=true');
    cache_clear_all($url, 'cache_ndla_grep');
  }

  /**
   * Used in courses
   */
  public static function get_courses($ids) {
    $data = array();
    $account = variable_get('ndla_grep_account', '');
    $response = self::_get('courses?education-group=' . $account);
    foreach($response->courses as $course) {
      if(!in_array($course->externalId, $ids)) {
        continue;
      }
      if($course->courseStructures[0]->setType == 'EDUCATIONAL_PROGRAM') {
        $name = self::_extract_name($course->courseStructures[0]->names);
        $link = $course->courseStructures[0]->link;
        $data[$name] = array(
          'name' => $name,
          'endpoint' => $link,
          'id' => $course->externalId,
        );
      }
    }
    ksort($data);
    return $data;
  }

  /**
   * Used in courses
   */
  public static function get_course($endpoint, $levels) {
    $data = array();
    $response = self::_get($endpoint);
    foreach($response->courseStructure->courseStructures as $courseStructure) {
      $name = self::_extract_name($courseStructure->names);
      $link = $courseStructure->links->self;
      foreach($courseStructure->levels as $level) {
        if(!in_array($level->id, $levels) && !empty($levels)) {
          continue;
        }
        if(!array_key_exists($level->id, $data)) {
          $data[$level->id] = array(
            'name' => self::_extract_name($level->names),
            'endpoint' => '',
            'children' => array(),
            'id' => $level->id,
          );
        }
        $data[$level->id]['children'][] = array(
          'name' => $name,
          'endpoint' => $link,
          'id' => $courseStructure->id,
        );
      }
    }
    ksort($data);
    return $data;
  }

  /**
   * Used in courses
   */
  public static function get_course_structure($endpoint) {
    $data = array();
    $response = self::_get($endpoint);
    foreach($response->courseStructure->curriculumSets as $set) {
      if($set->setType == 'PROGRAM') {
        $link = $set->links->self;
        break;
      }
    }
    $response = self::_get($link);
    foreach($response->curriculumSet->curriculums as $curriculum) {
      $name = self::_extract_name($curriculum->names);
      $link = $curriculum->links->self;
      $data[$name] = array(
        'name' => $name,
        'endpoint' => $link,
        'id' => $curriculum->id,
        'extra_id' => $response->curriculumSet->id,
      );
    }
    ksort($data);
    return $data;
  }

  /**
   * Used in courses
   */
  public static function get_aims($endpoint, $used = false) {
    $data = array();
    $response = self::_get($endpoint);
    foreach($response->curriculum->competenceAimSets as $sets) {

      if(empty($sets->competenceAims)) {
        /* Two levels of competance aim sets */
        foreach($sets->competenceAimSets as $set) {
          $name = self::_extract_name($set->names);
          $weight = $set->sortingOrder;
          if(!array_key_exists($weight . ':' . $name, $data)) {
            $data[$weight . ':' . $name] = array(
              'name' => $name,
              'id' => $set->id,
              'aims' => array(),
            );
          }
          foreach($set->competenceAims as $aim) {
            $aim_name = self::_extract_name($aim->names);
            $data[$weight . ':' . $name]['aims'][$aim->sortingOrder . ':' . $aim_name] = array(
              'name' => $aim_name,
              'id' => $aim->id,
            );
            ksort($data[$weight . ':' . $name]['aims']);
          }
        }
      } else {
        /* One level of competance aim sets */
        $name = self::_extract_name($sets->names);
        $weight = $sets->sortingOrder;
        if(!array_key_exists($weight . ':' . $name, $data)) {
          $data[$weight . ':' . $name] = array(
            'name' => $name,
            'id' => $sets->id,
            'aims' => array(),
          );
        }
        foreach($sets->competenceAims as $aim) {
          $aim_name = self::_extract_name($aim->names);
          $data[$weight . ':' . $name]['aims'][$aim->sortingOrder . ':' . $aim_name] = array(
            'name' => $aim_name,
            'id' => $aim->id,
          );
          ksort($data[$weight . ':' . $name]['aims']);
        }
      }
    }
    ksort($data);
    return $data;
  }

  /**
   * Used in common courses
   */
  public static function get_common_courses($ids) {
    $data = array();
    $response = self::_get('courses?education-group=fyr');
    foreach($response->courses as $course) {
      if(!in_array($course->externalId, $ids)) {
        continue;
      }
      if($course->courseStructures[0]->setType == 'CURRICULUM') {
        $response2 = self::_get($course->courseStructures[0]->link);
        $response3 = self::_get($response2->courseStructure->curriculumSets[0]->links->self);
        $name = self::_extract_name($response3->curriculumSet->curriculums[0]->names);
        $link = $response3->curriculumSet->curriculums[0]->links->self;
        $data[$name] = array(
          'name' => $name,
          'endpoint' => $link,
          'id' => $course->courseStructures[0]->id,
          'extra_id' => array($course->externalId, $response3->curriculumSet->id),
        );
      }
    }
    ksort($data);
    return $data;
  }

  /**
   * Used in common courses
   */
  public static function get_curriculum($endpoint, $levels, $sets) {
    $data = array();
    $response = self::_get($endpoint);
    foreach($response->curriculum->competenceAimSets as $competenceAimSet) {
      if(!empty($sets) && !in_array($competenceAimSet->id, $sets)) {
        continue;
      }
      $in_levels = FALSE;
      foreach($competenceAimSet->levels as $level) {
        if(in_array($level->id, $levels) || empty($levels)) {
          $in_levels = $level->id;
          break;
        }
      }
      if($in_levels == FALSE) {
        continue;
      }
      $name = self::_extract_name($competenceAimSet->names);
      $data[$name] = array(
        'name' => $name,
        'endpoint' => $competenceAimSet->links->self,
        'id' => $competenceAimSet->id,
        'extra_id' => $in_levels,
      );
    }
    ksort($data);
    return $data;
  }

  /**
   * Used in common courses
   */
  public static function get_aim_set($endpoint) {
    $data = array();
    $response = self::_get($endpoint);
    foreach($response->competenceAimSet->competenceAimSets as $set) {
      $name = self::_extract_name($set->names);
      $weight = $set->sortingOrder;
      if(!array_key_exists($weight . ':' . $name, $data)) {
        $data[$weight . ':' . $name] = array(
          'name' => $name,
          'aims' => array(),
          'id' => $set->id,
          'endpoint' => $set->links->self,
        );
      }
      foreach($set->competenceAims as $aim) {
        $aim_name = self::_extract_name($aim->names);
        $data[$weight . ':' . $name]['aims'][$aim->sortingOrder . ':' . $aim_name] = array(
          'name' => $aim_name,
          'id' => $aim->id,
        );
        ksort($data[$weight . ':' . $name]['aims']);
      }
    }
    ksort($data);
    return $data;
  }

  /**
   * Used in common courses
   */
  public static function get_aim_list($endpoint, $used = false) {
    $data = array();
    $response = self::_get($endpoint);
    foreach($response->competenceAimSet->competenceAims as $aim) {
      $name = self::_extract_name($aim->names);
      $weight = $aim->sortingOrder;
      if(!array_key_exists($weight . ':' . $name, $data)) {
        $data[$weight . ':' . $name] = array(
          'name' => $name,
          'id' => $aim->id,
        );
      }
    }
    ksort($data);
    return $data;
  }

  public static function get_psi($identifier) {
    global $base_url, $base_path, $user;
    $psi = variable_get('ndla_grep_psi', '');
    if(empty($psi)) {
      $psi = $base_url . $base_path;
    }
    return $psi . $identifier;
  }

}
