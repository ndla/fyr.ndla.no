<?php

function ndla_grep_fyr_search() {
  drupal_set_title('');
  $items = array(
    'Courses',
    'Common courses',
  );
  $data = array();
  foreach($items as $item) {
    $data[t($item)] = array(
      'data' => t($item),
      'class' => array(str_replace(' ', '-', strtolower($item))),
    );
  }
  ksort($data);
  $types = theme('item_list', array('items' => $data));

  $data = array();
  $ids = ndla_grep_fyr_get_all_course_ids();
  $courses = NdlaGrepFyrClient::get_courses($ids);
  foreach($courses as $key => $course) {
    $data[$key] = array(
      'data' => $course['name'],
      'endpoint' => $course['endpoint'],
      'callback' => '/ndla_grep/search/ajax/courses/course',
      'class' => array('all courses'),
      'onclick' => 'ndla_grep_search_update(this);',
      'grep_id' => $course['id'],
    );
  }  
  $common_courses = NdlaGrepFyrClient::get_common_courses($ids);
  foreach($common_courses as $key => $course) {
    $data[$key] = array(
      'data' => $course['name'],
      'endpoint' => $course['endpoint'],
      'callback' => '/ndla_grep/search/ajax/common/curriculum',
      'class' => array('all common-courses'),
      'onclick' => 'ndla_grep_search_update(this);',
      'grep_id' => $course['extra_id'][0],
    );
  }

  
  $curriculum = theme('item_list', array('id' => 'ndla_grep_type', 'items' => $data));


  drupal_add_css(drupal_get_path('module', 'ndla_grep_fyr') . '/css/ndla_grep_fyr.search.css', 'header');
  drupal_add_js(drupal_get_path('module', 'ndla_grep_fyr') .'/js/ndla_grep_fyr.search.js', 'file');
  drupal_add_css('//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css','external');

  return "<div id='ndla_grep_search_0' class='ndla_grep_search_container'>" . $types . "</div><div id='ndla_grep_search_1' class='ndla_grep_search_container'>" . $curriculum . "</div><div id='ndla_grep_search_2' class='ndla_grep_search_container'></div><div id='ndla_grep_search_3' class='ndla_grep_search_container'></div><div id='ndla_grep_search_4' class='ndla_grep_search_container'></div>"; 
}

function ndla_grep_search_ajax_common_curriculum() {
  $parameters = drupal_get_query_parameters();
  $endpoint = $parameters['endpoint'];
  $grep_id = $parameters['grep_id'];
  $levels = ndla_grep_fyr_get_levels($grep_id);
  $sets = ndla_grep_fyr_get_competence_aims_sets($grep_id);
  $items = NdlaGrepFyrClient::get_curriculum($endpoint, $levels, $sets);
  $data = array();
  foreach($items as $item) {
    $data[] = array(
      'data' => $item['name'],
      'endpoint' => $item['endpoint'],
      'callback' => '/ndla_grep/search/ajax/common/sets',
      'onclick' => 'ndla_grep_search_update(this);',
    );
  }
  print theme('item_list', array('items' => $data));
}

function ndla_grep_search_ajax_common_sets() {
  $parameters = drupal_get_query_parameters();
  $items = NdlaGrepFyrClient::get_aim_set($parameters['endpoint']);
  $data = array();
  foreach($items as $item) {
    $data[] = array(
      'data' => $item['name'],
      'endpoint' => $item['endpoint'],
      'callback' => '/ndla_grep/search/ajax/common/aims',
      'onclick' => 'ndla_grep_search_update(this);',
    );
  }
  print theme('item_list', array('items' => $data));
}

function ndla_grep_search_ajax_common_aims() {
  global $base_path;
  $parameters = drupal_get_query_parameters();
  $items = NdlaGrepFyrClient::get_aim_list($parameters['endpoint'], true);
  $data = array();
  foreach($items as $item) {
    $data[] = '<a href="' . $base_path . 'search?f[0]=ndla_grep_competence_aims_id:' . $item['id'] . '">' . $item['name'] . '</a>';
  }
  print theme('item_list', array('items' => $data));
}

function ndla_grep_search_ajax_courses_course() {
  $parameters = drupal_get_query_parameters();
  $grep_id = $parameters['grep_id'];
  $levels = ndla_grep_fyr_get_levels($grep_id);
  $items = NdlaGrepFyrClient::get_course($parameters['endpoint'], $levels);
  $html = '';
  foreach($items as $year) {
    $title = $year['name'];
    $data = array();
    foreach($year['children'] as $item) {
      $data[] = array(
        'data' => $item['name'],
        'endpoint' => $item['endpoint'],
        'callback' => '/ndla_grep/search/ajax/courses/structure',
        'onclick' => 'ndla_grep_search_update(this);',
      );
    }
    $html .= theme('item_list', array('title' => $title, 'items' => $data));
  }
  print $html;
}

function ndla_grep_search_ajax_courses_structure() {
   $parameters = drupal_get_query_parameters();
   $items = NdlaGrepFyrClient::get_course_structure($parameters['endpoint']);
   $data = array();
   foreach($items as $item) {
     $data[] = array(
       'data' => $item['name'],
       'endpoint' => $item['endpoint'],
       'callback' => '/ndla_grep/search/ajax/courses/aims',
       'onclick' => 'ndla_grep_search_update(this);',
     );
   }
   print theme('item_list', array('items' => $data));
}

function ndla_grep_search_ajax_courses_curriculum() {
  global $base_path;
  $parameters = drupal_get_query_parameters();
  $items = NdlaGrepFyrClient::get_aims($parameters['endpoint'], true);
  foreach($items as $group) {
    $title = $group['name'];
    $data = array();
    foreach($group['aims'] as $aim) {
      $data[] = '<a href="' . $base_path . 'search?f[0]=ndla_grep_competence_aims_id:' . $aim['id'] . '">' . $aim['name'] . '</a>';
    }
    $html .= theme('item_list', array('title' => $title, 'items' => $data));
  }
  print $html;
};