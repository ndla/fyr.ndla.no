(function($) {
  $(function() {
    $('.node-form').click(function(e) {
      if($.browser.mozilla) {
        var $target = $(e.target);
      } else {
        var $target = $(e.toElement);
      }

      if($target.hasClass('aim-list-item')) {
        var endpoint = $target.data('endpoint');
        var target = $target.data('target');
        var callback = $target.data('callback');
        var tree = $target.data('tree');
        if($target.data('loading') == true) {
          return;
        }
        if($target.siblings('ul').length) {
          $target.siblings('ul').toggle();
          $target.find('i.fa').toggleClass('fa-angle-down').toggleClass('fa-angle-right');
        } else {
          $target.data('loading', true);
          $target.find('i.fa').removeClass('fa-angle-right').addClass('fa-spinner fa-spin');
          var $container = $target.parents('.ndla-course-selector');
          var type = $container.hasClass('courses') ? 'courses' : 'common_courses';
          var stored = $('input[name="field_' + type + '[ndla_grep_' + type + '_selected][data]"]').val();
          if(target == 'right') {
            $container.find('.right').html('<i class="fa fa-spinner fa-spin"></i>');
            $container.find('.aim-list-item.active').removeClass('active');
            $target.addClass('active');
          }
          $.ajax({
            url: callback,
            data: { endpoint : endpoint, tree : tree, stored : stored },
            success: function(data) {
              if(target == 'left') {
                $target.after(data);
                $target.find('i.fa').removeClass('fa-spinner fa-spin').addClass('fa-angle-down');
              } else if(target == 'right') {
                $container.find('.right').html(data);
              } else {
                alert(target);
              }
              $target.data('loading', false);
              $('input[name="field_' + type + '[ndla_grep_' + type + '][hidden]"]').val('<div class="ndla-course-selector clearfix ' + type + '">' + $container.html() + '</div>');
            }
          });
        }
      }
      if($target.hasClass('aim-list-checkbox')) {
        if($target.is(':checked')) {
          $target.attr('checked', true);
        } else {
          $target.removeAttr('checked');
        }
        var $container = $target.parents('.ndla-course-selector');
        var type = $container.hasClass('courses') ? 'courses' : 'common_courses';
        var tree = $target.data('tree');
        var stored = $('input[name="field_' + type + '[ndla_grep_' + type + '_selected][data]"]').val();
        $('input[name="field_' + type + '[ndla_grep_' + type + '][hidden]"]').val('<div class="ndla-course-selector clearfix">' + $container.html() + '</div>');
        $container.find('input').attr('disabled', true);
        $.ajax({
          url: Drupal.settings.basePath + 'ndla_grep/ajax/store',
          data: { stored : stored, tree : tree },
          success: function(data) {
            data = $.parseJSON(data);
            $('input[name="field_' + type + '[ndla_grep_' + type + '_selected][data]"]').val(data.json);
            $('input[name="field_' + type + '[ndla_grep_' + type + '_selected][hidden]"]').val(data.html);
            $('#ndla_grep_' + type + '_aim_selected').html(data.html);
            $container.find('input').attr('disabled', false);
          }
        });
      }
      if($target.hasClass('aim-list-remove')) {
        var type = $target.parents('#ndla_grep_common_courses_aim_selected').length ? 'common_courses' : 'courses';
        var $container = $('.ndla-course-selector.' + type);
        var id_list = $target.data('tree');
        var stored = $('input[name="field_' + type + '[ndla_grep_' + type + '_selected][data]"]').val();
        var id = $(id_list).last()[0];
        var checkboxes = $('#' + id, $container);
        checkboxes.each(function() {
          var $checkbox = $(this);
          var tree = $checkbox.data('tree');
          for(i in id_list) {
            if(id_list[i] != tree[i]['_id']) {
              return;
            }
          }
          $checkbox.removeAttr('checked');
        });
        $('input[name="field_' + type + '[ndla_grep_' + type + '][hidden]"]').val('<div class="ndla-course-selector clearfix ' + type + '">' + $container.html() + '</div>');
        $.ajax({
          url: Drupal.settings.basePath + 'ndla_grep/ajax/remove',
          data: { stored : stored, id_list : id_list },
          success: function(data) {
            data = $.parseJSON(data);
            $('input[name="field_' + type + '[ndla_grep_' + type + '_selected][data]"]').val(data.json);
            $('input[name="field_' + type + '[ndla_grep_' + type + '_selected][hidden]"]').val(data.html);
            $('#ndla_grep_' + type + '_aim_selected').html(data.html);
          }
        });
      }
    });
  });

})(jQuery);