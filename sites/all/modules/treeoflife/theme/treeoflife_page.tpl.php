<script type="text/javascript">
  var TreeOfLife = TreeOfLife || {};
  TreeOfLife.hotspots = <?php print $hotspots ?>
</script>

<div data-role="page" id="mappage">
  <div data-role="content" data-theme="d">
    <div id="treeoflife-map"></div>
  </div>

  <div data-role="footer" data-position="fixed" data-tap-toggle="false">
    <a href="#keypage" data-icon="info" data-role="button" data-direction="reverse"><?php print t('Keys') ?></a>
    <a href="#searchpage" data-icon="search" data-role="button" data-transition="slideup" data-rel="dialog"><?php print t('Search') ?></a>
  </div>
  <div id="navigation" data-role="controlgroup" data-type="vertical">
    <a href="#" data-role="button" data-icon="plus" id="plus"
       data-iconpos="notext"></a>
    <a href="#" data-role="button" data-icon="minus" id="minus"
       data-iconpos="notext"></a>
  </div>
</div>
<div data-role="page" id="searchpage" data-theme="d">
  <div data-role="header">
    <h1><?php print t('Search for organism') ?></h1>
  </div>
  <div data-role="content">
    <input type="text" id="treeoflife-search"
           value="" placeholder="<?php print t('Search for organism') ?>"/>
  </div>
  <ul data-role="listview" data-inset="true" id="search_results"></ul>
</div>

<div data-role="page" id="infopage" data-theme="a">
  <div data-role="content">
    <div id="treeoflife-title" class="treeoflife"></div>
    <div id="treeoflife-text" class="treeoflife clearfix"></div>
  </div>
   <div data-role="footer" data-position="fixed">
    <a href="#mappage" data-icon="home" data-role="button" data-direction="reverse"><?php print t('Back') ?></a>
  </div>
</div>
<div data-role="dialog" id="loadingpage" data-theme="a">
  <div class="dialog-content" data-role="content" data-theme="a">
    <p><?php print t('Tree of Life is an evolution tree based on the English') ?> <a href="http://www.open2.net/treeoflife/index.html" target="_window">The Open University</a>, <?php print t('and developed in cooperation with') ?> <a href="http://www.ndla.no" target="_window">NDLA</a> &amp; <a href="http://www.amendor.no" target="_window">Amendor</a>.</p>
    <p id="progressstatus"><?php print t('Loading data') ?></p>
    <a class="dialog-button" href="#mappage" data-role="button" data-theme="a"><?php print t('Start') ?></a>
  </div>
</div>
<div data-role="page" id="keypage">
  <div id="keypage-content" data-role="content">
    <div data-role="collapsible-set" data-theme="a">
    </div>
  </div>
   <div data-role="footer" data-position="fixed">
    <a href="#mappage" data-icon="home" data-role="button"><?php print t('Back') ?></a>
  </div>
</div>
