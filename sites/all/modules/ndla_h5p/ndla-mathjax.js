(function ($) {
  if (MathJax === undefined) {
    return; // Missing MathJax
  }

  // Hide annoying processing messages
  MathJax.Hub.Config({messageStyle: 'none'});

  $(document).ready(function () {
    // Find H5P content
    $('.h5p-content').each(function (i, e) {
      var doJax = function (node) {
        MathJax.Hub.Queue(['Typeset', MathJax.Hub, node]);
      };
      var MutationObserver = window.MutationObserver || window.WebKitMutationObserver;
      if (!MutationObserver) {
        var check = function () {
          $('math', e).each(function (j, m) {
            doJax(m.parentNode);
          });
          checkInterval = setTimeout(check, 2000);
        };
        var checkInterval = setTimeout(check, 2000);
      }
      else {
        var observer = new MutationObserver(function (mutations) {
          for (var i = 0; i < mutations.length; i++) {
            for (var j = 0; j < mutations[i].addedNodes.length; j++) {
              if (mutations[i].addedNodes[j].nodeType === Node.ELEMENT_NODE) {
                doJax(mutations[i].addedNodes[j]);

              }
            }
          }
        });
        observer.observe(e, {
          childList: true,
          subtree: true
        });
      }
    });
  });
})(H5P.jQuery);
