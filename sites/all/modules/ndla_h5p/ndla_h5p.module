<?php

/**
 * @file
 *  ndla_h5p.module php file
 *  Drupal module ndla_h5p. Modifies the h5p module to work with the NDLA site.
 */

/**
 * Implements hook_form_alter().
 */
function ndla_h5p_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id === 'h5p_content_node_form') {
    // Always add custom settings after form is built
    $form['#after_build'][] = 'ndla_h5p_content_node_form_after_build';
  }
}

/**
 * After form build callback.
 *
 * Adds the custom ndla-wiris2 button to ckeditors.
 */
function ndla_h5p_content_node_form_after_build($form, &$form_state) {
  $settings = array(
    'h5peditor' => array(
      'assets' => array(
        'js' => array(
          drupal_get_path('module', 'ndla_h5p') . '/ndla-wiris2.js'
        )
      ),
    ),
  );
  drupal_add_js($settings, 'setting');

  return $form;
}

/**
 * Implementation of hook_h5p_semantics_alter().
 *
 * Adds mathml tags to wysiwyg fields.
 */
function ndla_h5p_h5p_semantics_alter(&$semantics) {
  foreach ($semantics as $field) {
    // Lists specify the field inside the list.
    while ($field->type === 'list') {
      $field = $field->field;
    }

    if ($field->type === 'group') {
      ndla_h5p_h5p_semantics_alter($field->fields);
    }
    else if ($field->type === 'text' && isset($field->widget) && $field->widget === 'html') {
      if (!isset($field->tags)) {
        $field->tags = array();
      }

      // Add MathML tags
      $field->tags = array_merge($field->tags, array(
        'span',
        'math',
        'maction',
        'maligngroup',
        'malignmark',
        'menclose',
        'merror',
        'mfenced',
        'mfrac',
        'mglyph',
        'mi',
        'mlabeledtr',
        'mlongdiv',
        'mmultiscripts',
        'mn',
        'mo',
        'mover',
        'mpadded',
        'mphantom',
        'mroot',
        'mrow',
        'ms',
        'mscarries',
        'mscarry',
        'msgroup',
        'msline',
        'mspace',
        'msqrt',
        'msrow',
        'mstack',
        'mstyle',
        'msub',
        'msup',
        'msubsup',
        'mtable',
        'mtd',
        'mtext',
        'mtr',
        'munder',
        'munderover',
        'semantics',
        'annotation',
        'annotation-xml',
      ));
    }
  }
}

/**
 * Implements hook_h5p_scripts_alter().
 *
 * Adds MathJax support.
 */
function ndla_h5p_h5p_scripts_alter(&$scripts, &$libraries, &$embed_type) {
  $scripts[] = (object) array(
    'path' => 'http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML',
    'version' => ''
  );
  $scripts[] = (object) array(
    'path' => drupal_get_path('module', 'ndla_h5p') . '/ndla-mathjax.js',
    'version' => '?ver=0.0.1'
  );
}
