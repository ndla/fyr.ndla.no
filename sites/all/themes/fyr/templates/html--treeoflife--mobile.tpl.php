<!DOCTYPE html>
<html lang="<?php print $language->language ?>" xml:lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
<head>
  <?php print $head ?>
  <title><?php print $head_title ?></title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <?php  print $styles ?>
  <?php  print $scripts ?>
</head>
  <body class="mobile">
    <?php print $page; ?>
  </body>
</html>
