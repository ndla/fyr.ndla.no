<?php
//http://drupalcontrib.org/api/drupal/drupal--modules--node--node.tpl.php
//node--[CONTENT TYPE].tpl.php
//$content['field_name']['#theme'] = "nomarkup";
//hide($content['field_name']);
if ($classes) {
  $classes = ' class="'. $classes . ' "';
}

if ($id_node) {
  $id_node = ' id="'. $id_node . '"';
}

hide($content['comments']);
hide($content['links']);
hide($content['field_keywords']);
hide($content['field_language']);
?>

<!-- node.tpl.php -->
<article <?php print $id_node . $classes .  $attributes; ?> role="article">
  <?php print $mothership_poorthemers_helper; ?>

  <?php print render($title_prefix); ?>
  <?php if (!$page): ?>
    <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>" rel="bookmark"><?php print $title; ?></a></h2>
  <?php endif; ?>
  
  <?php print render($title_suffix); ?>

  <?php if ($display_submitted): ?>
  <footer>
    <?php if($status == 0): ?>
      <span class='node-unpublished'><i class="fa fa-exclamation-triangle"></i> <?php echo t('Unpublished node'); ?></span>
    <?php endif ?>

    <span class="metadata">
      <?php print t('Shared by:'); ?> <?php print $name; ?>
      -
      <?php print t('Published:'); ?> <time><?php print format_date($variables['created'], 'norwegian'); ?></time>
      -
      <?php print t('Updated:'); ?> <time><?php print format_date($variables['changed'], 'norwegian'); ?></time>
    </span>
    

    <?php if(module_exists('comment')): ?>
      <span class="comments"><?php print $comment_count; ?> Comments</span>
    <?php endif; ?>

  </footer>
  <?php endif; ?>



  <div class="content">
    <?php print render($content['links']); ?>
    <?php print render($content);?>
  </div>


  <?php print render($content['comments']); ?>
</article>
