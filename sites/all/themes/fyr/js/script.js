(function ($) {
  $(function() {
    jQuery('.share #edit-bundle').chosen({width: "250px", disable_search_threshold: 20})

    jQuery('.block-views-substart-slideshow-block').find('.contextual-links-wrapper').remove();
    
    var $block = jQuery('.block-views-substart-slideshow-block');
    if($block.length > 0) {
      if(typeof(jQuery.fn.slick) == "function") {
        jQuery('.block-views-substart-slideshow-block').slick({
          autoplay: true,
          autoplaySpeed: 5000,
          speed: 1000
        });
      }
    }
  });
})(jQuery);