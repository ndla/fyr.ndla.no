<?php

function deling_preprocess_block(&$vars) {
  $vars['classes_array'][] = "block";
}

function deling_preprocess_html(&$vars) {
  $vars['classes_array'][] = 'deling';
}

function deling_preprocess(&$vars, $hook) {
  $headers = drupal_get_http_header();
  if (isset($headers['status'])) {
    if($headers['status'] == '404 Not Found'){
      $key = array_search('page__404', $vars['theme_hook_suggestions']);
      if($key) {
        unset($vars['theme_hook_suggestions'][$key]);
      }
    }
  }
}

function deling_preprocess_page(&$vars,$hook) {
  $vars['site_name'] = variable_get('site_name', '');
}